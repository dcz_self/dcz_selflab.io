<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Git-botch-email</title>
    <meta http-equiv="Refresh" content="0; URL=https://dorotac.eu/posts/git-botch-email" />
    <link rel="stylesheet" href="/style.css" />
  </head>
  <body>
    <section id="notice">
      This site moved to <a href="https://dorotac.eu/posts/git-botch-email">dorotac.eu</a>. The version you're seeing now is a preserved copy from April 2023.
    </section>
    <main>

  <article>
    
      <h1># Git-botch-email</h1>
<p>I don't really like git-send-email. I avoid projects that use it, if I can.</p>
<p>It could have been much better, but I (thankfully) use it rarely enough that I can't come up with its list of sins on the spot, and I don't end up getting my arguments acknowledged.</p>
<p>Today (2021-09-24) I have the dreadful need to send a patch to the Linux kernel, and I'm going to make the most of it by lining up my thoughts. Hopefully someone takes it to heart and creates a `<code>git-sends-email-better</code>`.</p>
<h2>## Email is all right</h2>
<p>Don't get me wrong, I like the decentralization aspect of email, I like that it's based on open standards, that I can use an arbitrary client to send it, and that I don't have to run random code on my computer just to submit or review patches. Forges fail at those, to various extents.</p>
<h2>## But git adds its own quirks</h2>
<p>I have a tree with my commit on top: it changed 3 lines. I want someone to include my changes on top of the official tree. Let's assume that I read the project's contribution guide, and I know that I need to use git-send-email, and I also know where to send the change. Let's begin.</p>
<pre>```
<code>$ git send-email 
No patch files specified!
git send-email [options] &lt;file | directory | rev-list options &gt;
[...]
</code>```</pre>
<p>&quot;patch files&quot;? I thought git operated on the basis of commits and trees. Why would I want to send a patch file using git? Anyway, there's a &quot;rev-list&quot; possiblity, so we can proceed with that:</p>
<pre>```
<code>$ git send-email 00082f898de21fd5ebb28dc561c173f6fde8e44a
/tmp/7yZAWWiApC/0001-media-imx-Fix-rounding.patch
To whom should the emails be sent (if anyone)?
</code>```</pre>
<p>I answer the questions, and then I get:</p>
<pre>```
<code>Send this email? ([y]es|[n]o|[e]dit|[q]uit|[a]ll):
</code>```</pre>
<p>Lol, no. It's my first submission, and I want to review it. Possibly by giving it to someone else first. I'm only human, and I make mistakes.</p>
<p>But where's the &quot;save&quot; option?</p>
<p>Okay, never mind. Maybe there is a &quot;save&quot; option further along the way. After all, I didn't give git any email access yet – I just want a dry run. It's not going to throw away the edits it offers you to perform, right?</p>
<pre>```
<code>Send this email? ([y]es|[n]o|[e]dit|[q]uit|[a]ll): y
sendmail: Cannot open mail:25
Died at /usr/libexec/git-core/git-send-email line 1497, &lt;FIN&gt; line 3.
</code>```</pre>
<p>…Actually, it totallydid throw them away. Thankfully I didn't make any edits (j/k, the first time in my life I tried to send a patch, I followed a tutorial and already wrote a heap of text at this step. That hurt).</p>
<p>Do you know how the cat keeps meowing but you have no idea what it wants? If only it could speak human language.</p>
<p>If only git-send-email could speak human language.</p>
<p>Some searching later, it turns out that, indeed, git wanted to have access to my SMTP server to send the email itself.</p>
<h3>### Git sending emails</h3>
<p>Let's get back to the save fiasco. I have a perfectly cromulent email client which can import .mbox or .eml files, and send them on. It's already configured for my email server, it can sign my emails, I trust it with my passwords, and it's customized to save sent emails to IMAP.</p>
<p>Why can't I save my git patch, and load it into my email client, to send it with my GPG signature?</p>
<p>Why should I trust git with full access to my email outbox? Why shouldn't I be able to use my usual Mail User Agent (MUA) to do the sending in a way that I pre-approved, respecting the security level I want to maintain, including TLS versions? Email is based on standards, after all.</p>
<p>I just hope Git doesn't handle the email password itself, given its <a href="https://nvd.nist.gov/vuln/detail/CVE-2020-5260">track</a> <a href="https://cve.mitre.org/cgi-bin/cvekey.cgi?keyword=git">record</a>.</p>
<p>Oh, and don't forget that if you have a fancy multi-identity setup in your MUA, you have to duplicate it with git-send-email. Sure, git has a global config file. But If I configured that to my work account, then I'm one mistake away from sending a personal contribution to some other random repo from my work email. Double difficulty if you contribute under multiple different contexts.</p>
<p>So I'm stuck at having to <a href="https://www.freedesktop.org/wiki/Software/PulseAudio/HowToUseGitSendEmail/">configure</a> every repo which demands git-send-email separately (use `<code>--local</code>` instead of `<code>--global</code>`. With git-send-email, opsec is harder.</p>
<p>Just give me the damn save already!</p>
<h3>### Acceptance</h3>
<p>Okay, I guess I have little choice in the matter. *<em>Sigh</em>*. Let's change the email to point to myself as the addressee. This will obviously not be a dry run for the final email because of the address mismatch, but hopefully nothing starts on fire when we change the addressee later.</p>
<p><img src="sent_email.png" alt="sent email" /></p>
<p>Great, it seems to look … odd. I was not expecting the patch to be part of the message. How am I supposed to download and apply it? By copying and pasting? It makes sense: those who reply can easily add their comments inline. I guess I can live with it. As long as I don't change the contents of the patch, it should be possible to extract.</p>
<h3>### Intermission</h3>
<p>But wait, git-send-email allows you to edit the message before sending, and mess it up in any way you want?!?</p>
<p><img src="botched_email.png" alt="screenshot of an email with an extra &quot;From:&quot; header in a terminal editor" /></p>
<pre>```
<code>Send this email? ([y]es|[n]o|[e]dit|[q]uit|[a]ll): y
OK. Log says:
[...]
Result: 250
</code>```</pre>
<p>LOL, git-send-email apparently doesn't do any validation. I hope that email didn't actually get forwarded anywhere. I put in &quot;none&quot; in the address field, but I dont really know well enough how git-send-email works. I've seen it add addresses. That wouldn't have worried me with my MUA.</p>
<h3>### Back to the draft</h3>
<p>Oh, right, maybe I can actually edit the draft from my MUA this time! If git-send-email doesn't try to stop me from being an idiot, I have no reason to use it now.</p>
<p>I copied the email from inbox to drafts, and added the timely message.</p>
<h3>### Intermission 2</h3>
<blockquote>
<p>&gt; Sometimes it's convenient to annotate patches with some notes that are not meant to be included in the commit message. [...] Such messages can be written below the three dashes &quot;---&quot; that are in every patch after the commit message.</p>
</blockquote>
<p>So, if I want to include extra context inside my email, I should cram it in between lines of computer-readable text and hope for the best?</p>
<blockquote>
<pre>```
<code>The changes from 451a7b7815d0b pass 2^n to round_up, while n is needed.

Fixes: 451a7b7815d0b
---
Hi, I tested the patch on the Librem 5.
drivers/staging/media/imx/imx-media-utils.c | 6 +++---
1 file changed, 3 insertions(+), 3 deletions(-)

diff --git a/drivers/staging/media/imx/imx-media-utils.c b/drivers/staging/media/&gt; imx/imx-media-utils.c
index 5128915a5d6f..a2d8fab32a39 100644
</code>```</pre>
</blockquote>
<p>That must be the best way ever invented to ensure people accidentally alter the wrong thing.</p>
<h3>### Applying the patch</h3>
<p>I was hoping not to have to go through the ordeal, but I don't want to bother my co-workers with my non-work blog. I'm being too nice.</p>
<p>I was joking when I said &quot;copy-paste&quot; the email content, and yet, this is how <a href="https://stackoverflow.com/a/51995810">Stack Overflow</a> deals with it. And while I'm finding a bunch of guides for sending, that's the only answer dealing with receiving git-send-email results. Alas.</p>
<p>Surprisingly, it worked-for-me.</p>
<p>A couple exchanged emails later, and… I need to change the commit contents. Since the change was small, I just winged it, and altered the email directly, but if I actually had to go through the entire ordeal of git-send-email, I'd lose my changes again.</p>
<h3>### Revisions</h3>
<p>Finally, my email went to review. Of course, it didn't come out unscathed. I had to submit a second version, which consisted of multiple commits.</p>
<p>Here again, git-send-email showed how being careful results in paper cuts: each commit is sent as a separate email. That would have been okay of I could just load them into my draft folder and send from there, but, as we established earlier, the only way to do that is to send the email to myself. That means I have to change the recipient list for each email in the series manually. What botheration!</p>
<h2>## More tool problems</h2>
<p>Even after the patch was sent successfully, it's still less than a commit. It does not preserve the history of how the author got there. The base tree is discarded, and only the diff remains. Without an external convention, the reviewer does't even know which tree to apply the patch on, much less where it was originally tested. It's impossible to merge trees using git-send-patch either. There's not much &quot;git&quot; in it, really, because commits and trees are what git is made of.</p>
<h2>## Linux problems</h2>
<p>There's a separate class of problems that are not the consequence of the tool, but the consequence of the culture which is often associated with the tool. It's fixable without any software changes, but it needs changes to wetware – which is probably even more difficult.</p>
<p>Here's a loose list, based on the Linux kernel:</p>
<p>How to find the correct address to direct the patches? How to find the correct tree to apply the patch on? Magical incantations are sometimes required like &quot;Signed-off-by:&quot; that have a meaning other than its constituent words (no, it doesn't mean your email is signed). Those things are documented, but they are not universal, and not obvious. And they are not difficult in the same way as writing a kernel patch is difficult – those are pure bureaucracy overhead, which does not have any bearing on code quality.</p>
<p>More overhead is in supporting ancient clients that can't handle MIME or compression. Those are banned in the kernel, instead of fixing clients to handle compression and inline disposition. Another exercise to support inflexible tools at the expense of human effort.</p>
<p>And the mother of all pet peeves: hard-wrapping of commit messages. Do you prefer to read prose by incessant scrolling, or with nonsense lines? Random example from the Linux kernel mailing list:</p>
<p><a href="scroll.webm">Monospaced text hard-wrapped wider than the display, reader scrolls every line horizontally</a></p>
<p><img src="wrapping.png" alt="Prose with lines wrapped to random widths" /></p>
<p>This is what reading a patch would look like on the Librem 5 – depending on how you prefer to suffer – and it's also what any other pre-formatted text looks like. Including your commit messages. Have mercy and don't do hard wrapping.</p>
<h2>## It can be better!</h2>
<p>Those are, in the end, not unsurmountable problems, at least not all of them. *<em>When used together with a competent email client</em>*, git-send-email does its job passably. The only paper cut is not being able to export the emails without sending them.</p>
<p>It gets worse if there's no competent email client. Then the task of editing, reviewing, saving for later and coming back, and fixing mistakes falls entirely on the same tool. As far as I can tell, it's rather tedious, and the ability to get an overview is rather poor. There's no way to save and resume at all.</p>
<p>I'm not going to give it a score better than &quot;passable&quot; even with this papered over: it remains a fact that git-send-email almost encourages the submitter to accidentally mess up the patch by entering text in the wrong place. Until the payload is clearly delineated from the cover letter (as an inline attachment?), this cannot be solved.</p>
<hr />
<h2>## **<strong>Some time later</strong>**</h2>
<p>One of my patches was eventually accepted. The rest have been picked up by some poor soul who has been trying to get them upstreamed for the past 2 months.</p>
<p>git-send-email didn't get in the way any more, but I still messed up. I missed some feedback in my inbox, and thought the patches were completely forgotten, until the new person stepped in. You might blame my way of reading email, and you would be right: my email inbox is a mess. I've been trying to fix it for the past 5 years with little success. Meanwhile, I rarely lose feedback when it's placed on a web page, because it organizes conversations in a sensible manner.</p>
<p>Thankfully, that problem is not inherent to email, and has been solved by <a href="https://sr.ht/">sourcehut</a>. It closes the gap between an email-only workflow and a web-only forge by providing a single contact point, and displaying the knowledge (including historical data) in a decent manner. My analysis wouldn't be complete without mentioning it, and I ask projects using git-send-email to adopt it or something similar: I can't handle naked emails, and I'm not the only one!</p>
<p>But, being an internet service, sourcehut doesn't fix the problems in git-send-email. Perhaps they could come up with an improve version of that tool? I sure hope they could.</p>

    
    <footer>
      <p>
        Written on <time datetime="2022-03-13">2022-03-13</time>.
      </p>
    </footer>
    <section>
        <a href="https://comments.dorotac.eu/posts/git-botch-email">Comments</a>
        <iframe src="https://comments.dorotac.eu/posts/git-botch-email" style="width: 100%;height: 100vh;" loading="lazy">
            <a href="https://comments.dorotac.eu/posts/git-botch-email">Comments</a>
        </iframe>
    </section>
  </article>

    </main>
    <section id="global">
        <p><a href="/">dcz's projects</a></p>
        <p>Thoughts on software and society.</p>
        <div>
          <nav>
  <p>posts:</p>
  <ul>
    
        <li><a href="/posts/switch">Domain change</a></li>
    
        <li><a href="/posts/quick_maps">Maps à la carte</a></li>
    
        <li><a href="/posts/holes">Spots on the Sun and worn-out clothes</a></li>
    
        <li><a href="/posts/remote_debugging">Graphical debugging on the Librem 5 using QtCreator</a></li>
    
        <li><a href="/posts/jazda_rust">Jazda: Rust on my bike</a></li>
    
        <li><a href="/posts/why">Why Jazda?</a></li>
    
        <li><a href="/posts/git-botch-email">Git-botch-email</a></li>
    
        <li><a href="/posts/potion">A potion of experience</a></li>
    
        <li><a href="/posts/tabcheology">Browser tab archaeology</a></li>
    
        <li><a href="/posts/rust_maemo">Rust on Maemo</a></li>
    
        <li><a href="/posts/REing a pen">Reverse engineering a pen</a></li>
    
        <li><a href="/posts/in_the_middle">Start in the middle</a></li>
    
        <li><a href="/posts/nerd">When being a nerd paid off</a></li>
    
        <li><a href="/posts/flame_graph">Linux kernel flame graphs</a></li>
    
        <li><a href="/posts/fluwid">Simulating fluwids</a></li>
    
        <li><a href="/posts/blossom">A beautiful blossom of engineering excellence</a></li>
    
        <li><a href="/posts/objective">My objective function will blow it up</a></li>
    
        <li><a href="/posts/flat_earth">Geo in GL part 1: Flattening Earth</a></li>
    
        <li><a href="/posts/input_method">Wayland and input methods</a></li>
    
        <li><a href="/posts/embeddings">Word embedding fun</a></li>
    
        <li><a href="/posts/not_keyboard">It&#39;s not about keyboards</a></li>
    
        <li><a href="/posts/cargo_versions">Why I avoid Cargo: dependency versions</a></li>
    
        <li><a href="/posts/gcc_symbols">From C to exec: Part 3</a></li>
    
        <li><a href="/posts/dynamic_linking">From C to exec: Part 2</a></li>
    
        <li><a href="/posts/linking">From C to exec</a></li>
    
        <li><a href="/posts/brutalism">Brutalist blogging</a></li>
    
  </ul>
</nav>
          <p><a href="/atom.xml">Atom feed (no longer updated)</a></p>
        </div>
        <nav>
          <p>lists:</p>
          <ul>
            <li><a href="/">about me</a></li>
            <li><a href="/forges">my code</a></li>
          </ul>
        </nav>
    </section>
    <footer>
      <p>Articles published under the CC-BY-SA 4.0 license.</p>
    </footer>
  </body>
</html>