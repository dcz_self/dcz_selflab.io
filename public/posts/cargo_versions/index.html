<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Why I avoid Cargo: dependency versions</title>
    <meta http-equiv="Refresh" content="0; URL=https://dorotac.eu/posts/cargo_versions" />
    <link rel="stylesheet" href="/style.css" />
  </head>
  <body>
    <section id="notice">
      This site moved to <a href="https://dorotac.eu/posts/cargo_versions">dorotac.eu</a>. The version you're seeing now is a preserved copy from April 2023.
    </section>
    <main>

  <article>
    
      <h1># Why I avoid Cargo: dependency versions</h1>
<p>Ever since I started making use of <a href="https://doc.rust-lang.org/cargo/">Cargo</a> to build the Rust pieces of <a href="https://source.puri.sm/Librem5/squeekboard/">Squeekboard</a>, I've hit a wall whenever I needed to add something nontrivial to the build process. I haven't documented them before, so whenever I complained about Cargo, I ended up looking ridiculous without having anything to show for the complaints.</p>
<p>Last week I spent three days solving a problem with building Squeekboard that should have been solveable in 30 minutes, in large part due to Cargo.</p>
<h2>## Buster and Bullseye</h2>
<p>Squeekboard is a crucial part of the software stack of the <a href="https://puri.sm/products/librem-5/">Librem5</a> mobile phone. It's primary goal is to fit in well in <a href="https://pureos.net/">PureOS</a>, which powers the phone. PureOS is in turn based on Debian, and inherits a lot of its practices, which are followed by projects related to the Librem5, including Squeekboard.</p>
<p>One such practice is that Rust crates are vendored by Debian, and not by the application author. They are installable with `<code>apt</code>`, and end up in a local repository in `<code>/usr/share/cargo/registry</code>`.</p>
<p>Librem5's base operating system is moving from Debian 10 to Debian 11. I was <a href="https://source.puri.sm/Librem5/squeekboard/-/issues/200">asked</a> to make Squeekboard work on both in the transition period. Debian 10 ships with gtk-0.5.0, whereas Debian 11 ships with gtk-0.7.0, which contain some incompatibilities, and Squeekboard's build process must adjust to them, depending on where the build happens.</p>
<p>Piece of cake: there's one variable, which needs to be turned into one decision. I did this a million times. Little did I know Cargo hates simple solutions.</p>
<h2>## What's the version we're using?</h2>
<p>It's not unusual for projects to support two versions of the same dependency. Perhaps the versions come from different vendors. C programs don't have trouble with this:</p>
<pre>```
<code>#include &lt;gtk.h&gt;
#if GTK_VERSION==0.5
  // old stuff
#endif
</code>```</pre>
<p>Rustc won't know dependency versions by itself, but Cargo should turn it to something like this:</p>
<pre>```
<code>#[cfg(dependency.gtk.version = &quot;0.5.*&quot;)]
use gio::SimpleActionExt; // Doesn't exist in later versions of gtk
</code>```</pre>
<p>But… I haven't managed to find any equivalent. It makes sense, Cargo can pull several copies of the same crate, and I think their pieces can even be used in the same files if one is not careful. So there's no good reason this should have worked. Fair, let's try something else.</p>
<h2>## What's the available version?</h2>
<p>If the compilation process can't tell us what versions we're dealing with, perhaps we can check that before compilation. In Meson, we'd do something like this:</p>
<pre>```
<code>gtk-rs = dependency(&quot;gtk-rs&quot;)
if gtk.version().startswith(&quot;0.5.&quot;)
      add_project_arguments('--cfg old_gtk', language : 'rust')
endif
</code>```</pre>
<p>And then, in Rust:</p>
<pre>```
<code>#[cfg(&quot;old_gtk&quot;)]
use gio::SimpleActionExt; // Doesn't exist in later versions of gtk
</code>```</pre>
<p>Now the only remaining thing is to create the dependency lookup procedure. While Squeekboard is Debian-focused, building on non-Debian systems is still important, so it must build with crates.io. Thankfully, we have `<code>cargo search</code>`. Let's try with a vendored registry:</p>
<pre>```
<code>root@c684b7b31b07:/# CARGO_HOME=/mnt/build/eekboard/debian/cargo/ cargo search gtk
error: dir /usr/share/cargo/registry does not support API commands.
Check for a source-replacement in .cargo/config.
</code>```</pre>
<p>Oh no. We can forget about it. I'm not willing to write a tool that searches for crates in all the ways that Cargo supports, and I'm honestly boggled that Cargo doesn't properly do it itself.</p>
<p>As a bonus, the output of cargo search just doesn't make sense. I'm looking for the regex crate, which is at version &quot;1.3.9&quot;:</p>
<pre>```
<code>$ cargo search regex=1
combine-regex-1 = &quot;1.0.0&quot;      # Re-export of regex 1.0 letting combine use both 0.2 and 1.0
webforms = &quot;0.2.2&quot;             # Provides form validation for web forms
</code>```</pre>
<p>Totally useless :(. But I still have a few tricks up my sleeve.</p>
<h2>## Use a build flag</h2>
<p>Isn't this obvious? Let's skip all that dependency detection, and just order the build system to use one, in the dumbest fashion possible. The way you'd do it with Meson:</p>
<pre>```
<code>if get_option(&quot;legacy&quot;) == true
  gtk-rs = dependency(&quot;gtk-rs&quot;, version=0.5)
  add_project_arguments('--cfg old_gtk', language : 'rust')
else
  gtk-rs = dependency(&quot;gtk-rs&quot;, version&gt;0.5)
endif
squeekboard = executable('squeekboard',
  dependencies: [gtk-rs],
)
</code>```</pre>
<p>Add to that the conditional in the Rust file, and we're done. Cargo is not Meson, but we can call it with flags too, and then instruct it to use the right dependency. Right?</p>
<p>Wrong.</p>
<p>While Cargo allows choosing dependency versions, they are selected based on target, not on flags. You can use:</p>
<pre>```
<code>[target.'cfg(unix)'.dependencies]
openssl = &quot;1.0.1&quot;
[target.'cfg(not(unix))'.dependencies]
openssl = &quot;1.0.0&quot;
</code>```</pre>
<p>You can remember that the `<code>cfg()</code>` syntax supports features:</p>
<pre>```
<code>[target.'cfg(feature=&quot;legacy&quot;)'.dependencies]
gtk=&quot;0.5.*&quot;
[target.'cfg(not(feature=&quot;legacy&quot;))'.dependencies]
gtk=&quot;0.7.*&quot;
</code>```</pre>
<p>but then you see this:</p>
<pre>```
<code># cargo build
warning: Found `feature = ...` in `target.'cfg(...)'.dependencies`. This key is not supported for selecting dependencies and will not work as expected. Use the [features] section instead: https://doc.rust-lang.org/cargo/reference/features.html
</code>```</pre>
<p>And when you follow that web page, you learn that you can specify other dependencies, but not other versions. Foiled again!</p>
<p>Again, I'm boggled why such a basic piece of functionality is working in such a complicated and restrictive way. Wouldn't it be easier to abolish the weird `<code>feature = [dep]</code>` syntax and instead let `<code>foo.dependencies</code>` work, with all the fine-grained control over what the dependencies are?</p>
<h2>## Embedded crates</h2>
<p>But I didn't stop there. If the deps can't be specified the usual way, then let's get there through a back door. If I can't choose a version, I will choose a crate. 0.5 turns into a crate called &quot;legacy&quot;, and 0.7 into one called &quot;current&quot;. My Cargo.toml looked like this:</p>
<pre>```
<code>[dependencies]
current = {path=&quot;deps/current&quot;, optional=true}
old = {path=&quot;deps/old&quot;, optional=true}

[features]
legacy = [&quot;old&quot;]
</code>```</pre>
<p>The `<code>deps/old/Cargo.toml</code>` contained the actual version:</p>
<pre>```
<code>[dependencies]
gtk-rs = &quot;0.5.*&quot;
</code>```</pre>
<p>and the current one had `<code>0.7.*</code>`. When we choose the crate, we choose the dependency with it! So clever! There's no way it won't work!</p>
<pre>```
<code>root@c684b7b31b07:~/foo/foo# CARGO_HOME=/mnt//build/eekboard/debian/cargo/ cargo build
error: failed to select a version for the requirement `gtk = &quot;0.5.*&quot;`
  candidate versions found which didn't match: 0.7.0
  location searched: directory source `/usr/share/cargo/registry` (which is replacing registry `https://github.com/rust-lang/crates.io-index`)
required by package `old v0.1.0 (/root/foo/foo/deps/old)`
    ... which is depended on by `foo v0.1.0 (/root/foo/foo)`
</code>```</pre>
<p>What!? This cannot be! Why oh why?</p>
<p>My only guess is that cargo pulls all the dependencies indiscriminately, regardless of whether it actually needs them or not. Obviously, this entire shebang is because we *<em>don't</em>* have one of them! Why is Cargo missing the point of choosing dependency versions so hard?</p>
<h2>## Nuclear option</h2>
<p>With this in mind, there's only one solution left. If Cargo is greedy enough to snatch everything it sees, then we'll just not let it know there are possibly any other dependencies. We'll generate a Cargo.toml after we know which dependency we need, and we'll never let Cargo know about the other dependency. The details of the generation are quite straightforward: just change depedencies based on build flags. The build scripts get complicated, though. Now they must include `<code>--manifest-path</code>`. This looks trivial, but this changes the root of the crate, so now we need to give it the path to the sources again:</p>
<pre>```
<code>[lib]
name = &quot;rs&quot;
path = &quot;@path@/src/lib.rs&quot;
crate-type = [&quot;staticlib&quot;, &quot;rlib&quot;]

# Cargo can't do autodiscovery if Cargo.toml is not in the root.
[[bin]]
name = &quot;test_layout&quot;
path = &quot;@path@/src/bin/test_layout.rs&quot;

[[example]]
name = &quot;test_layout&quot;
path = &quot;@path@/examples/test_layout.rs&quot;
</code>```</pre>
<p>But it works. Finally.</p>
<p>Sadly, we sacrificed autodetection of tests and binaries, as well as distro-agnosticism.</p>
<h2>## You're doing it all wrong!</h2>
<p>I'm sure some of you will see the struggle and consider it self-inflicted. &quot;Why didn't you try X?&quot;, &quot;You're fighting the tool because your model is outdated!&quot;, &quot;This is not a bug&quot;. Some of them will be valid criticisms, and I'm going to address those I could think of.</p>
<h3>### Use multiple Cargo.lock versions</h3>
<p>I could have used &quot;*&quot; as the dependency version, and instead rely on two versions of Cargo.lock to select the actual dependency I want. That is troublesome.</p>
<p>I don't want to know exactly what versions of Rust crates (or any other deps really) the upstream distro is shipping. This is why I use distros in the first place: they remove some burden of deps auditing from me. All I want to know is the minor version for API compatibility reasons. However, Cargo.lock forces me to care about dependencies by asking for a hash of each. I would have to find out what dependency is available, and feed Cargo.lock with its hash. That hits the same problem with detecting what we have again.</p>
<h3>### Vendor your crates yourself</h3>
<p>Squeekboard is just one project out of many in PureOS, and all the others follow Debian's best practices: use Debian's software versions whenever possible. On one hand, this is designed to set right expectations, on the other it relegates the responsibility for auditing and updating to Debian, as explained in the previous argument.</p>
<p>In this light, I'm already letting Debian vendor my crates, and that won't change.</p>
<h3>### Cargo is designed for crates.io, not for distributions or local repositories</h3>
<p>I'm not sure if this is Cargo's explicit goal to be useful with crates.io, but my experience says other sources are in practice playing catch-up. If I relied exclusively on crates.io, I would have no problems.</p>
<p>However, the build policy in PureOS requires builds to happen without a network connection. That means we can't use crates.io. We've already eliminated vendoring before, and so we're stuck with some form of a local repository, which clearly isn't well-supported by Cargo.</p>
<h2>## Insights</h2>
<p>My insight from this adventure is that Cargo doesn't prioritize users who want to control their own sources of software at the moment. It's inflexible in the way that favours crates.io, where the implicit assumption is that all possible crates and versions are available. After all, it crashes when a non-available version is specified even if unused.</p>
<p>Cargo also doesn't want application developers to offload dependency checking. It's recommended to commit Cargo.toml together with the application, but there's no provision to ignore it when doing a build with local versions of the same crates, like when you do in distro builds.</p>
<p>Cargo is not composable the way other build systems are. When building a C program, artifacts in the form of shared or static libraries are placed in a well-known directory. Cargo creates an opaque directory structure for compiled crates in the `<code>target</code>` directory, which cannot be used by another build system later due to lack of documentation. Sadly, this means that when Cargo fails to solve a need, there's no alternative.</p>
<p>The same creates a network effect, where Cargo is de facto the only tool that builds useful Rust programs. It's difficult to the point of pointlessness to &quot;build&quot; serde and gtk-rs separately, and then &quot;link&quot; them with the main program using manual `<code>rustc</code>` calls.</p>
<p>Creating composable artifacts would undermine Cargo's monopoly and allow a better integration with distributions as well as other programming languages in my opinion. Build systems should not infect all software they touch.</p>

    
    <footer>
      <p>
        Written on <time datetime="2020-06-27">2020-06-27</time>.
      </p>
    </footer>
    <section>
        <a href="https://comments.dorotac.eu/posts/cargo_versions">Comments</a>
        <iframe src="https://comments.dorotac.eu/posts/cargo_versions" style="width: 100%;height: 100vh;" loading="lazy">
            <a href="https://comments.dorotac.eu/posts/cargo_versions">Comments</a>
        </iframe>
    </section>
  </article>

    </main>
    <section id="global">
        <p><a href="/">dcz's projects</a></p>
        <p>Thoughts on software and society.</p>
        <div>
          <nav>
  <p>posts:</p>
  <ul>
    
        <li><a href="/posts/switch">Domain change</a></li>
    
        <li><a href="/posts/quick_maps">Maps à la carte</a></li>
    
        <li><a href="/posts/holes">Spots on the Sun and worn-out clothes</a></li>
    
        <li><a href="/posts/remote_debugging">Graphical debugging on the Librem 5 using QtCreator</a></li>
    
        <li><a href="/posts/jazda_rust">Jazda: Rust on my bike</a></li>
    
        <li><a href="/posts/why">Why Jazda?</a></li>
    
        <li><a href="/posts/git-botch-email">Git-botch-email</a></li>
    
        <li><a href="/posts/potion">A potion of experience</a></li>
    
        <li><a href="/posts/tabcheology">Browser tab archaeology</a></li>
    
        <li><a href="/posts/rust_maemo">Rust on Maemo</a></li>
    
        <li><a href="/posts/REing a pen">Reverse engineering a pen</a></li>
    
        <li><a href="/posts/in_the_middle">Start in the middle</a></li>
    
        <li><a href="/posts/nerd">When being a nerd paid off</a></li>
    
        <li><a href="/posts/flame_graph">Linux kernel flame graphs</a></li>
    
        <li><a href="/posts/fluwid">Simulating fluwids</a></li>
    
        <li><a href="/posts/blossom">A beautiful blossom of engineering excellence</a></li>
    
        <li><a href="/posts/objective">My objective function will blow it up</a></li>
    
        <li><a href="/posts/flat_earth">Geo in GL part 1: Flattening Earth</a></li>
    
        <li><a href="/posts/input_method">Wayland and input methods</a></li>
    
        <li><a href="/posts/embeddings">Word embedding fun</a></li>
    
        <li><a href="/posts/not_keyboard">It&#39;s not about keyboards</a></li>
    
        <li><a href="/posts/cargo_versions">Why I avoid Cargo: dependency versions</a></li>
    
        <li><a href="/posts/gcc_symbols">From C to exec: Part 3</a></li>
    
        <li><a href="/posts/dynamic_linking">From C to exec: Part 2</a></li>
    
        <li><a href="/posts/linking">From C to exec</a></li>
    
        <li><a href="/posts/brutalism">Brutalist blogging</a></li>
    
  </ul>
</nav>
          <p><a href="/atom.xml">Atom feed (no longer updated)</a></p>
        </div>
        <nav>
          <p>lists:</p>
          <ul>
            <li><a href="/">about me</a></li>
            <li><a href="/forges">my code</a></li>
          </ul>
        </nav>
    </section>
    <footer>
      <p>Articles published under the CC-BY-SA 4.0 license.</p>
    </footer>
  </body>
</html>