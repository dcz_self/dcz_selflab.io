---
title: "Jazda: Rust on my bike"
date: "2022-09-05"
---

Jazda: Rust on my bike
================

*This blog post is an improved version of an impromptu talk I gave at FrOSCon two weeks ago.*

I like to do things that don't quite make sense. One of them is putting rust on my bicycle.

![The front of a dirty purple bicycle. In the background, windows of a building which carries the FrOSCon logo.](bike.png)

This is my bicycle, and it's made out of steel. It has some rust already, but it's not the kind of "rust" I want to talk about.

![The seat tube shown from above. On the outside, it's covered with light dirt, and on the inside, the colour is dirty orange](tube.png)

I put Rust the programming language on my bike.

Bicycle computer
---------------------

I bought my first bicycle computer before I could even program. It was a simple mechanical device that is mounted near the wheel. It counts wheel rotations, and displays a distance.

![A metal box with a counter behind a window. On the left, there's a propeller-like element. On the bottom there's a mounting bracket. The display is made of 4 black 0s and one red ring to the right. The red digit reads 3, and partially rotated away, so that the bottom is not in the window's frame.](mechanical.png)

My next upgrade was an electronic device, one unlike what you could buy today. Those have more functionality: they may display distance, speed, and time travelled. I used one for over a decade, and it was possibly the best spent 20 EUR in my life.

![A squareish plastic display unit. It has a wide button on the top edge and another one on the bottom edge. In between, taking most of the space, is a transparent window with a LCD display. 7-segment digits show "0": one on the top labelled "KMH, and 3 on the bottom reading "0.00 TRP"](electronic.png)

Two things happened between then and now. One is that I learned to code. The other is that the battery ran out. Instead of replacing the battery, I came to the obvious conclusion: I can code, so I can make a better one!

Hardware
------------

I've been struggling to design the hardware for my bike computer for years. Sadly, I suck at hardware design. I can't solder, I can't design a plastic case, I can't build a device that won't shake apart during a most peaceful ride. It became clear that if I want a bike computer, I should start with existing hardware.

Meanwhile, the situation in the outside world slowly changed. Smartphones gained popularity, Internet of Things followed, smart watches started utilizing the new tiny and powerful components. Bicycle computers started taking advantage of the new powers too.

I'm much better at programming than at hardware, so I stopped to consider: should I take advantage of that influx of hardware, and adapt an existing device to my needs?

I could slap a smartphone on the handlebars, write an application and call it a day. Or I could find a smaller device, more like the bicycle computer I retired, and use it as the base for my software. But which path should I choose?

![A rounded-rectangular gadget, showing a table of values on a black-and-white LCD: speed, stopwatch, distance, time, heart rate, cadence. It has 4 small buttons, 2 on the left and 2 on the right.](modern.png)

There are many cyclists in the world. There are those who commute to work every day, others who go on family trips, some cycle to deliver things, and yet some like to race. They have different goals and needs while cycling, and each is interested in something different from a cycling computer.

This time, I'm building one that makes *me* happy. Which of my needs as a cyclist can a bike computer help with?

I usually cycle in places I roughly know, so I don't need a map, or a big, fragile screen to display one. I want to time my rides, so I want a screen that's big enough to show a few numbers in a big font, and the bike computer must be sturdy enough when I push the tempo over rocky trails. When I cycle, I leave my phone at home, so I won't miss anything if the bike computer is 100% offline. I draw on maps for OpenStreetMap, so I want some sensors like GPS, and a place to store their data. Thankfully, handling sensors and statistics doesn't require much computational power.

A smartphone meets those needs, but has some important downsides. Most smartphones aren't well readable in sunlight. They have short battery life – days compared to weeks – and they are not very resilient compared to other gadgets. I often get caught in the rain, and I'm pretty sure too much road dust or violent shaking is not healthy for smartphones – even if they don't come flying out of the harness.

The conclusion is pretty clear: I would be better served by a dedicated device. And so I started my search.

However, I'm only a single hacker, with limited time and abilities. I want to build a bike computer from scratch, I have another need – as a hacker, not cyclist: the device must be simple enough so that I can hack on it myself.

And I did see some interesting examples, like [one based on Linux](https://www.reddit.com/r/linuxmasterrace/comments/ehn19r/how_to_hack_stages_cycling_dash_l50_or_m50/). I rejected this one purely because I was afraid about the hardware complexity – if the manufacturer decided they needed a full-blown OS, then there must be a lot of hardware to manage there.

Finally, I found one. The [Bangle.jS 2](https://www.espruino.com/Bangle.js2) smart watch checked most of the boxes: lots of sensors, Bluetooth Low Energy, a screen readable in the sunlight, and even a GPS receiver! It only has one button, and it can't make sounds, but it was reverse engineered, and ready to flash with custom software. It was then or never: if I didn't use this as my base, I would probably never finish the bike computer project.

Rust
-----------

There are lots of choices in the embedded space. Operating systems like [Nuttx](https://nuttx.apache.org/), [Espruino](https://www.espruino.com/), [Mbed OS](https://os.mbed.com/mbed-os/), [Riot](https://www.riot-os.org/), [Zephyr](https://www.zephyrproject.org/). Or even running on bare metal. But my goal was clear:

![[Rust-lang](https://www.rust-lang.org/) logo](rust-logo-blk.svg)

I wrote enough C code to know I don't want to use it. The 2 Rust-enabled options were Riot and bare metal using [rust-embedded](https://github.com/rust-embedded) crates.

Actually, there were 3 options. At the last moment, I discovered [Tock](https://tockos.org/), an OS written entirely in Rust. It has an advantage over all other options (except Espruino): it's a pre-emptive operating system, able to load applications at runtime.

And loading new apps is a standard function of your computer, of your smart phone, and some smart watches. Why isn't this standard on bike computers yet? Puzzling, but if the sports gadgets manufacturers won't do that, I gladly will.

Tock project
-----

![Diagram showing 2 layers: Tock kernel and multiple apps on top: speed meter written in Rust, clock written in C, and space for more.](apps.svg)

Applications made for Tock are native code, can be written in C or Rust. Because of the multiprocessing architecture of Tock, we can split functionality into apps, like a speed meter, or a clock, and not worry how buggy they are: they may crash all they want, but they are separated from each other, so a crashing clock won't bring your down speed display.

Imagine a future where people load applications from the internet on the bike computer. Being able to just ignore a crashing app will be absolutely necessary.

But this kind of safety comes with a cost. You have to write a lot more code to abstract hardware resources. Instead of writing just one device driver, you actually need to write 3 pieces: one kernel driver, one userspace driver, and one multiplexer.

![Diagram showing the 3 layers: hardware, kernel, app. Hardware is the GMC303 chip. On top in the kernel, GMC303 driver underlies the compass syscall driver. On top o that, as part of the map app, there's the compass API](stack.svg)

Status
---------

Despite the slower pace, I managed to put Tock OS on the Bangle.js 2 hardware rather quickly. I started with a demo displaying speed:

![A smart watch on a wooden background. It reads "14", and there's a 30° arc to the left of the number, centered on the number. There's a small "33" in the lower right corner.](speed.png)

The demo is part of the [Jazda project](http://jazda.org) (which is what I called the bike computer), and it shows that the display stack is working, and that the GPS stack is working.

There's still lots of work before the grand vision can be realized. The main parts are:
- Bluetooth support
- Concurrency: there are compiler shortcomings that prevent this part of the OS from really gaining speed
- Communication between apps
- Communication with a computer
- Publishing apps online: a website, payment service

Jazda started as a hobby project, so I never forget about fun projects:
- [Seismos](https://gitlab.com/dcz_self/seismos), the sensor collection file system
- [Ray-graphics](https://framagit.org/jazda/core/-/tree/master/ray-graphics), the [SDF](https://www.ronja-tutorials.com/post/034-2d-sdf-basics/)-based graphics library
- Skitram, the unpublished time-series data compression library

GPS vs BLE
--------------

Currently, the greatest shortcoming of Jazda is the lack of Bluetooth Low Energy. This is a wireless protocol normally used by bicycle sensors. What Jazda is using right now is GPS readings. Unfortunately, GPS is rather energy-hungry, which means the device can only display speed for 5 hours before turning off, and the readouts aren't very accurate, either.

Tock has a strict policy of not allowing direct hardware access, so I can't just snatch an external BLE stack. If I did that, I would be stuck maintaining the result myself, because it wouldn't be accepted upstream. Alternatively, I could write a BLE stack myself, but Bluetooth is notoriously difficult to implement correctly.

For now, I'll just keep working on the other parts.

Innovation
-------------

Thankfully, speed readouts are overrated. There are plenty of other things that can be done without them. I collected some ideas:

- drawing a situational map
- wheelie detection
- surface roughness scanning for OpenStreetMap
- sonar mode for chasing a recorded ride
- ???
- profit

Some of them come from myself, some were suggestions I heard. Perhaps I won't be able to implement them all, but that's fine, because Jazda is open source software. Anyone is allowed to implement any crazy idea without signing an NDA and without the need to find a job at a sports equipment company. Having a shower thought and some perseverance is all that's necessary.

Community
---------------

But it's always easier to hack when other people can help, so you're invited to join our chat on Matrix or IRC: #jazda:[libera.chat](https://web.libera.chat/) . You can also reach out to me there to order development kits for Jazda. Those come with a unique breakout board to make it easy to reflash the Bangle.js 2 smart watch with the Jazda firmware.

![A USB programmer connected via a ribbon cable to a breakout board, connected to a USB cable that ends outside of the picture.](devkit.png)

If you're a hardware hacker, you're especially welcome. Perhaps you could help us build a future version of Jazda, without *any* shortcomings… and without useless heart sensors ;)

[![Jazda logo: "jz" stylized as a human on a bike](jazda.svg)](https://jazda.org)