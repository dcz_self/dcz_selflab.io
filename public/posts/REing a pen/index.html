<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Reverse engineering a pen</title>
    <meta http-equiv="Refresh" content="0; URL=https://dorotac.eu/posts/REing%20a%20pen" />
    <link rel="stylesheet" href="/style.css" />
  </head>
  <body>
    <section id="notice">
      This site moved to <a href="https://dorotac.eu/posts/REing%20a%20pen">dorotac.eu</a>. The version you're seeing now is a preserved copy from April 2023.
    </section>
    <main>

  <article>
    
      <h1># Reverse engineering a pen</h1>
<p>Like a lot of people, I have a favorite pen. I ran across it randomly, but I fell in love with it instantly. It glides across paper with barely any pressure, leaves strong, confident, and uniform streaks, and it's built like a tank to boot. But, unlike most pens that are worthy of appreciation, this pen is really many pens – it's disposable.</p>
<p><img src="pen.png" alt="A red ink Uni-Ball Eye pen" /></p>
<p>While the ink is bright an saturated, and there's a selection of colours, the Uni-Ball Eye is not meant to be refilled. To someone trying to reduce my waste production, using up one perfectly good pen every year is completely unacceptable. Knowing tricks back from the bad days of ink jet printers, I bought a vial of fountain pen ink, a syringe, and I set out to refill the carcass of my most recent pen.</p>
<p>Knowing that this is a blog post about refilling a pen, you might already guess that it wasn't as easy as it sounds.</p>
<h2>## Sesame open</h2>
<p>Before refilling, I destroyed a previous used up carcass to find best ways to inject liquid into the pen. It took me a while with the scissors – the outer shell is *<em>really</em>* thick – but eventually I found out that the back end has a thinner wall, and a needle did the job.</p>
<p>I also dissected the funny comb-like part near the tip that always looked like it filled with a little bit of ink. It's actually a second ink vessel, whose role I discover while refilling. Surprisingly, this vessel is not connected to the tip. There's a wick going from the tip straight to the big compartment, *<em>inside</em>* the tubular, comb-like second vessel.</p>
<p>Here's a diagram:</p>
<p><img src="diagram%20annotated.svg" alt="A diagram of a pen. From end to tip, a useless? hole leading to an empty container. A weak wall separating it from the ink container, which contains a thin wick leading all the way to the tip. The ink container ends in a permeable wall, which leads to a container defined by a &quot;comb&quot; structure. The &quot;comb&quot; is a pipe with the wick inside, and many flat rings on its outside. The rings are not reaching the container wall. Two guessed passages connect the end of this container with two &quot;nostrils&quot; near the tip." /></p>
<p>(The walls are much thicker in reality.)</p>
<h2>## Shooting it up</h2>
<p>For the first refill, I simply poked a hole in the back of the pen, placed the conjoined pen and syringe horizontally, and squirted in a few drops of the liquid. The wick became flooded with ink, and I could write again! But… all the ink slowly seeped into the comb container, leaving the wick dry after a minute. Even worse, two &quot;nostrils&quot; near the tip readily released ink as long as the comb was full. What's going on?</p>
<p><img src="comb.png" alt="Comb compartment" /> This comb container on a new pen is already partially filled. The red streaks are ink.</p>
<p>An obvious culprit is gravity, but the ink wasn't flowing all at once, so there must be more to it.</p>
<p>The comb shape drew my attention. Being composed mostly of walls, it gives the liquid plenty of tight spaces to which it can stick. Did the manufacturer intend to make use of the capillary effect? It would help liquid flow into the comb.</p>
<p>There was the open question of whether the pen was originally airtight, but adding pressure into considerations would make the problem too complicated, so I followed the next obvious lead.</p>
<h2>## Capillary effect</h2>
<p>If I could prevent the comb compartment from filling up, ink would stay in the main vessel, and I would write indefinitely. The only difference, apart from the opened end, between my refill and the original, was the ink used. Perhaps the original ink was tuned specifically for this pen design. Hoping to alter the liquid to be less eager to climb up the comb, I tried a couple mixes with glycerin and water.</p>
<h2>## Nosebleed</h2>
<p>I tried those tests also with the syringe hole plugged. That's when something unusual happened: the nostrils produced bubbles of air, like a kid who dropped an ice cream. Clearly, that was not just gravity's fault: air doesn't flow out under its own weight.</p>
<p>The story of fountain pens leaking inside airplanes came to my mind. Perhaps this pen had similar considerations?</p>
<h2>## Air pressure</h2>
<p>I had reasons to believe that yes, they do. First off, why would a pen need &quot;nostrils&quot; connecting to the ink compartment, if not to equalize pressure? Coupled with the comb compartment, this made sense. When the pen is in an upright position, ink flows towards the tip, and there's a pocket of air near the butt. That air would expand when surrounding air pressure drops, and push out ink from the main vessel. The comb area would contain the excess, and the comb structure itself would prevent it from flowing out.</p>
<p>Every time I tried to use the pen in the morning, it cried me tears of ink, flourishing my handwriting in a special way. Pressure is the hint. Mornings are cold. My hands are warm. Gases expand with temperature. Air pushes out ink. It all makes sense!</p>
<h2>## Flawed methodology</h2>
<p>Sadly, my tests so far were all flawed: I never allowed the comb to dry up so far, and if the pressure theory was right, then the pen could work correctly only when the comb had plenty of empty space. I came up with a new procedure:</p>
<ul>
<li>use the pen until all the ink was released – via the tip or the nostrils</li>
<li>carefully plug the injection point.</li>
</ul>
<p>If my theory was right, then the following would happen:</p>
<ul>
<li>the liquid would have enough surface tension to not flood the comb after a few hours</li>
<li>the wick would stay submerged in ink, and never dry up</li>
<li>writing on cold mornings would make the comb fill up a little</li>
<li>but it would not be enough to fill up the comb and cause spills.</li>
</ul>
<h2>## Experiments</h2>
<p><img src="blot.png" alt="A red ink blot" /> My daily companion when researching this article: the ink blot.</p>
<p>The plan didn't work out flawlessly, and every attempt was about as fun as watching ink dry. That's because using up all ink took up ages, and the ink blots added a special flourish to my handwriting. I made several failures to plug the injection point: it turns out that just sticking a needle in there is not enough, and ink flows out the back. What's good enough is glueing it up using some plastic foil and liquid latex.</p>
<p><img src="plug.png" alt="The end of a pen plugged with a piece of paper, all soaked with red." /></p>
<p>While that works to keep the ink out the back end, it turns out that the ink still flows out the front! That invalidates my theory. Or does it?</p>
<p>I didn't want to seal the pen irreversibly in case that I needed to adjust something, so the end was probably letting air seep in a little. I left the pen overnight in different positions, and, after watching the ink in the comb change, concluded that gravity was responsible for the failure this time.</p>
<p>Without any better ideas, I went ahead and mixed in some glycerin into the ink vessel, which seems to have stopped the outflow. Success!</p>
<h2>## Inkblots</h2>
<p>That last experiment proved the minimal changes needed for the Uni-Ball Eye to be reuseable:</p>
<ul>
<li>use ink with about 10% glycerin</li>
<li>refill from the back while the comb compartment is not full</li>
<li>plug in the refill hole afterwards.</li>
</ul>
<p>The comb compartment is probably a way to avoid leaks due to pressure.</p>
<p><img src="comparison.png" alt="Two pens next to similar squiggles of red ink. The bottom is broader and brighter than the top one." /> The top pen in this picture is the refilled one. In reality, both inks look darker.</p>
<p>The ink I used is not as intense as the original one, and it doesn't flow so easily, but on the other hand, it's a bit thinner, and still flows decently enough. It can work as a cheap alternative to a new pen, which normally costs about 3 EUR per piece.</p>
<h2>## Lessons learned</h2>
<p>Turns out that there's a good deal of thought and complexity embedded in every day objects. Physical objects are take more effort to understand than as software. You need to control your environment, retrying the experiment can take a lot of time, and you may get yourself dirty in the middle. None of this will be a surprise to an experimental scientist, of course.</p>
<p>It's still a joy to explore, and the results are going to be a fair bit easier to explain to a random human – the outcome is satisfyingly concrete!</p>

    
    <footer>
      <p>
        Written on <time datetime="2021-07-15">2021-07-15</time>.
      </p>
    </footer>
    <section>
        <a href="https://comments.dorotac.eu/posts/REing a pen">Comments</a>
        <iframe src="https://comments.dorotac.eu/posts/REing a pen" style="width: 100%;height: 100vh;" loading="lazy">
            <a href="https://comments.dorotac.eu/posts/REing a pen">Comments</a>
        </iframe>
    </section>
  </article>

    </main>
    <section id="global">
        <p><a href="/">dcz's projects</a></p>
        <p>Thoughts on software and society.</p>
        <div>
          <nav>
  <p>posts:</p>
  <ul>
    
        <li><a href="/posts/switch">Domain change</a></li>
    
        <li><a href="/posts/quick_maps">Maps à la carte</a></li>
    
        <li><a href="/posts/holes">Spots on the Sun and worn-out clothes</a></li>
    
        <li><a href="/posts/remote_debugging">Graphical debugging on the Librem 5 using QtCreator</a></li>
    
        <li><a href="/posts/jazda_rust">Jazda: Rust on my bike</a></li>
    
        <li><a href="/posts/why">Why Jazda?</a></li>
    
        <li><a href="/posts/git-botch-email">Git-botch-email</a></li>
    
        <li><a href="/posts/potion">A potion of experience</a></li>
    
        <li><a href="/posts/tabcheology">Browser tab archaeology</a></li>
    
        <li><a href="/posts/rust_maemo">Rust on Maemo</a></li>
    
        <li><a href="/posts/REing a pen">Reverse engineering a pen</a></li>
    
        <li><a href="/posts/in_the_middle">Start in the middle</a></li>
    
        <li><a href="/posts/nerd">When being a nerd paid off</a></li>
    
        <li><a href="/posts/flame_graph">Linux kernel flame graphs</a></li>
    
        <li><a href="/posts/fluwid">Simulating fluwids</a></li>
    
        <li><a href="/posts/blossom">A beautiful blossom of engineering excellence</a></li>
    
        <li><a href="/posts/objective">My objective function will blow it up</a></li>
    
        <li><a href="/posts/flat_earth">Geo in GL part 1: Flattening Earth</a></li>
    
        <li><a href="/posts/input_method">Wayland and input methods</a></li>
    
        <li><a href="/posts/embeddings">Word embedding fun</a></li>
    
        <li><a href="/posts/not_keyboard">It&#39;s not about keyboards</a></li>
    
        <li><a href="/posts/cargo_versions">Why I avoid Cargo: dependency versions</a></li>
    
        <li><a href="/posts/gcc_symbols">From C to exec: Part 3</a></li>
    
        <li><a href="/posts/dynamic_linking">From C to exec: Part 2</a></li>
    
        <li><a href="/posts/linking">From C to exec</a></li>
    
        <li><a href="/posts/brutalism">Brutalist blogging</a></li>
    
  </ul>
</nav>
          <p><a href="/atom.xml">Atom feed (no longer updated)</a></p>
        </div>
        <nav>
          <p>lists:</p>
          <ul>
            <li><a href="/">about me</a></li>
            <li><a href="/forges">my code</a></li>
          </ul>
        </nav>
    </section>
    <footer>
      <p>Articles published under the CC-BY-SA 4.0 license.</p>
    </footer>
  </body>
</html>