---
title: "Why Jazda?"
date: "2022-06-05"
---

Why Jazda?
========

This is a cleaned up transcript of a lightning talk I gave at the Gulaschprogrammiernacht 20.

* * *

Have you ever had a project that you worked on for so long that you forgot why you're doing it? I was just asked "why are you doing this?" about a project I started 10 years ago, and on which I really started working this year. The project is called Jazda [https://jazda.org].

Why do I do it? It's an open source bicycle computer. There are a few of them, but not many. The general idea of a bicycle computer has been explored in so many ways. There's Garmin, which is almost a monopoly. You can also buy one at Kaufland for 10 euro. So why do you need another one?

If you take a fancy one like the Garmin, you'll find that it's not programmable. You cannot do it, you're not allowed to do it. If you take the Kaufland bike computer, it's just too simple. If you try to mount your phone on the bike, it's bulky and inconvenient. So yeah, I wanted something that I could program to get the four freedoms of software and to experiment with it.

There are other options. There are smart watches, like the PineTime, or Bangle.js. There is the Cyclotron, which is a simple bike computer that's also open source, including custom hardware. But I don't actually want any of those. Why not? First off, a bike computer needs to be readable in sunlight. Most smart watches can't do that. Bangle.js 2 can, but JavaScript? Sorry, I'm not a fan. The Cyclotron is the closest to what I want, but since I came all the way here, then maybe… let's not stop here?

If I use a simple single-purpose device, and connect it to the computer with a flasher to load software, it would be too technical, I would be basically the only person in the world interested in using it. What if I took the next step? What about a bicycle computer with apps?

Why should a bicycle computer be more difficult to use than an Apple phone or watch, or your Android? And what if you had a nice SDK to program it? Yeah, that's something to do.

Basically, this is the idea with Jazda. The project is still early, still not there, the goal has not been reached yet.

But you can already buy them from me, so I'm not still in the weeds, but there's still a lot of work required.

* * *

At the conference, my talk ended by inviting folks to talk to me and describing my hangout spot. It was a little awkward to explain.

Online, it's easier. I hang out on the IRC/Matrix channel #jazda:libera.chat . Stop by sometime!