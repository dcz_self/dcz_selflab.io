---
title: "Domain change"
date: "2023-04-14"
---

Domain change
=========

Please update your RSS feeds! I'm moving the blog over to [dorotac.eu](https://dorotac.eu/). I don't want to be dependent on gitlab's domain and hosting.

The website will remain at the old address for as long as possible, but will not be updated.