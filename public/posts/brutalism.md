---
title: "Brutalist blogging"
date: "2020-05-19"
---

Brutalist blogging
==================

I started this website in order to share bits of my knowledge. While I don't know everything, I have some workflows and opinions on a range of topics, from systems programming, through user interfaces, to collaboration. I hope that some of my insights will be interesting to others.

One of my insights is that computers are under-utilized. They are powerful, yet few people are able to command their power. The deluge of programming resources seems to be losing against the dwindling cues to do something novel with the machines. Today's phones neither come with BASIC, nor Turbo Pascal, apps don't lend themselves to modification via replacing config or art files, and Excel is not as ubiquitous as on desktops. The end result is closer to reading someone's books of magic than writing them.

As I started writing this Markdown document, I noticed that I was on the verge of the magical realm, and it was up to me to leave cracks in the magical armor, through which the inner workings of the post could be seen. I took inspiration from [brutalist architecture](https://en.wikipedia.org/wiki/Brutalist_architecture#Characteristics), which tries to be [structurally honest](https://medium.com/on-architecture-1/the-new-brutalism-6601463336e8):

> Whatever has been said about honest use of materials, most modern buildings appear to be made of whitewash or patent glazing, even when they are made of concrete or steel. Hunstanton appears to be made of glass, brick, steel and concrete, and is in fact made of glass, brick, steel and concrete. Water and electricity do not come out of unexplained holes in the wall, but are delivered to the point of use by visible pipes and manifest conduits. One can see what Hunstanton is made of, and how it works, and there is not another thing to see except the play of spaces.

In a parallel between architecture and computing, I want my work to stand out against mainstream technology by being more honest transparent, and less secretive and magical. Brutalist blogging, if you will.

The first property of this blog is its simplicity. Minimal CSS, HTML, little scripting, and no minification make it human-inspectable. I intend to add more transparency as time goes on: each entry's raw [CommonMark](https://commonmark.org/) source can be accessed by adding `.md` to the URL. And finally, the [HTML renderer](/bm.py) preserves some of the source markup, like `#` for headings, `>` for quotes, to let the reader see the inner workings even if it never occurred to them to ask about them.

As a strong believer in the notion that computers should empower their users, I hope that my little contribution helps dispel the magic.
