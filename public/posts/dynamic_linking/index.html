<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>From C to exec: Part 2</title>
    <meta http-equiv="Refresh" content="0; URL=https://dorotac.eu/posts/dynamic_linking" />
    <link rel="stylesheet" href="/style.css" />
  </head>
  <body>
    <section id="notice">
      This site moved to <a href="https://dorotac.eu/posts/dynamic_linking">dorotac.eu</a>. The version you're seeing now is a preserved copy from April 2023.
    </section>
    <main>

  <article>
    
      <h1># From C to exec: Part 2</h1>
<p>The <a href="/posts/linking/">last part</a> explored static linking, but stopped before actually executing our executable. If you think this was all the linking, you're in for a surprise!</p>
<p>This post is best followed using the code from the previous part.</p>
<h2>## Act 1: Unexpected symbol</h2>
<h3>### Scene 1: Undefined?</h3>
<p>Did you notice the lonely symbol in the last act? Let's call it forth:</p>
<pre>```
<code># gcc -c -o printer.o printer.c
# ar rcs libprinter.a printer.o
# nm libprinter.a

printer.o:
0000000000000000 T print_hello
                 U puts
</code>```</pre>
<p>That's a lonely &quot;Undefined&quot; symbol! And where did it even come from? Remember `<code>printer.c</code>`:</p>
<pre>```
<code>#include &lt;stdio.h&gt;
void print_hello(void) {
  puts(&quot;Hello World!\n&quot;);
}
</code>```</pre>
<p>We call `<code>puts</code>`, but we never define it… Previously, when we tried to use a symbol that we didn't define, gcc refused to finish the linking, but here it works:</p>
<pre>```
<code># gcc hello.o libprinter.a -o hello
# ./hello
Hello World!
</code>```</pre>
<p>What gives?</p>
<h3>### Scene 2: Deus ex library</h3>
<p>Before we solve the mystery, let me throw another hint towards the solution: `<code>puts</code>` is hidden in a library! More specifically, it's in libc:</p>
<pre>```
<code># nm /lib64/libc.so.6 | grep puts
[…]
0000000000073280 W puts
[…]
</code>```</pre>
<p>Here, &quot;W&quot; means it's a &quot;weak&quot; but defined symbol.</p>
<p>We know where the lost symbol is, and where it's missing from. But we still don't know the connection between the two places! The newest trace we have is the `<code>/lib64/libc.so.6</code>` file...</p>
<h3>### Scene 3: Dynamic libraries</h3>
<p>We're familiar with static libraries already. They have the `<code>.a</code>` file ending, and they are an ingredient for the executable, and are no longer used after linking. But another variety exists. Like static libraries, it contain symbols, but its usefulness extends beyond linking time, and into the actual execution. They are called *<em>dynamic libraries</em>*, and their file names traditionally end with `<code>.so</code>`, for &quot;shared object&quot;.</p>
<pre>```
<code># file /lib64/libc.so.6
/lib64/libc.so.6: symbolic link to libc-2.29.so
# file /lib64/libc-2.29.so
/lib64/libc-2.29.so: ELF 64-bit LSB shared object, x86-64, version 1 (GNU/Linux), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, BuildID[sha1]=51377236ad01808e26404c98faa41d72f11a46a5, for GNU/Linux 3.2.0, not stripped, too many notes (256)
</code>```</pre>
<p>`<code>libc.so.6</code>` is one of them.</p>
<h3>### Scene 4: Reconstruction</h3>
<p>If we want to make progress on the story of the misplaced `<code>puts</code>`, we need to understand dynamic libraries. Time to create our own!</p>
<p>We already created a static library before, so let's use the same procedure, but make a dynamic one this time:</p>
<pre>```
<code># gcc -fPIC -c printer.c -o printer.o
# gcc -shared printer.o -o libprinter.so
# file libprinter.so
libprinter.so: ELF 64-bit LSB shared object, x86-64, version 1 (SYSV), dynamically linked, BuildID[sha1]=dd2595216f9c10b317ab3dbece8450a31fcaf672, not stripped
# nm libprinter.so | grep hello
0000000000001109 T print_hello
</code>```</pre>
<p>The procedure is similar to building a static library, except packaging with `<code>ar</code>` was replaced by another linking step (with `<code>-shared</code>`), making it look more like creating an executable. The object file gets created in a slightly different way, with the `<code>-fPIC</code>` flag being obligatory.</p>
<p>Most importantly, we see our `<code>print_hello</code>` symbol as present! Let's link the whole program together:</p>
<pre>```
<code># gcc hello.o libprinter.so -o hello_dynamic
# file hello_dynamic
hello_dynamic: ELF 64-bit LSB executable, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, for GNU/Linux 3.2.0, BuildID[sha1]=5a8e0d151089682720545349da87b330ab089805, not stripped
</code>```</pre>
<p>So far so good, this looks just like our previous static binary. Let's make sure all is fine.</p>
<pre>```
<code># nm hello_dynamic | grep hello
                 U print_hello
# ./hello_dynamic
./hello_dynamic: error while loading shared libraries: libprinter.so: cannot open shared object file: No such file or directory
</code>```</pre>
<p>Uh oh! We lost `<code>print_hello</code>` along the way, and the program now tries to open the shared library we created. The one that contains `<code>print_hello</code>`! Could there be a connection? What made the program try to load a file if all we do is printing text?</p>
<h3>### Scene 5: Dynamic linker</h3>
<p>The culprit here is the dynamic linker. It's part of the operating system responsible for loading programs. Remember the `<code>interpreter</code>` part of our file descriptions?</p>
<pre>```
<code>[...] dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2 [...]
</code>```</pre>
<p>The interpreter's responsibility is to execute executables. It sets up basic resources, links them together if needed (!), and finally gives control to the program by running the entry point (in C that ends up being the function corresponding to the `<code>main</code>` symbol).</p>
<p>You may be surprised to see linking here again. Didn't we do enough of that already? We linked object files together with the dynamic library, and the dynamic library needed to be linked itself. Why again?</p>
<p>Well, that's because we chose to create a dynamic library instead of a static one. The static library's selling point is that its code is injected (at the time of linking) into the executable. The dynamic library's point is that its code is injected (at the time of execution) into the running program. The former option provides some degree of certainty that the program doesn't change, while the latter gives some flexibility: the dynamic library can get changed and updated without the need to recreate the executable.</p>
<p>When the interpreter complains about `<code>cannot open shared object file</code>`, it means that it needed the library for the linking step, but couldn't find it in the standard location. The `<code>ldd</code>` command lists all dynamic libraries that need to be linked together before running the executable:</p>
<pre>```
<code># ldd ./hello_dynamic
        linux-vdso.so.1 (0x00007ffc35be9000)
        libprinter.so =&gt; not found
        libc.so.6 =&gt; /lib64/libc.so.6 (0x00007fc874215000)
        /lib64/ld-linux-x86-64.so.2 (0x00007fc874419000)
</code>```</pre>
<p>Our shared library is `<code>not found</code>`, because it's not in one of the standard paths. Thankfully, we can tell the dynamic linker to look for extra libraries in the current directory:</p>
<pre>```
<code># LD_LIBRARY_PATH=`pwd` ldd ./hello_dynamic
        linux-vdso.so.1 (0x00007ff5f4e32000)
        libprinter.so =&gt; /home/rhn/libprinter.so (0x00007ff5f4e28000)
        libc.so.6 =&gt; /lib64/libc.so.6 (0x00007ff5f4c26000)
        /lib64/ld-linux-x86-64.so.2 (0x00007ff5f4e33000)
# LD_LIBRARY_PATH=`pwd` ./hello_dynamic
Hello World!
</code>```</pre>
<p>Great, we directed the computer to find our lost `<code>print_hello</code>` symbol inside a shared library!</p>
<h3>### Scene 6: Standard libraries</h3>
<p>The list coming from `<code>ldd</code>` is suspicious. In our final step using gcc, we linked `<code>hello.o</code>` together with `<code>libprinter.so</code>`. Where did the other dynamic libraries on the list come from?</p>
<p>The answer is: kernel and standard libraries.</p>
<p>If you look at our statically linked executable, you will see the same list:</p>
<pre>```
<code># ldd ./hello
        linux-vdso.so.1 (0x00007ffeda3d9000)
        libc.so.6 =&gt; /lib64/libc.so.6 (0x00007f4e3293a000)
        /lib64/ld-linux-x86-64.so.2 (0x00007f4e32b3e000)
</code>```</pre>
<p>The libraries here are required for the execution of virtually all programs, therefore gcc includes them implicitly. If you remember all the way back, we found `<code>puts</code>` in one of them: in `<code>libc.so.6</code>`. It turns out that all our executables do eventually link with that library, just well after the executable is created. That's why we were allowed to use `<code>puts</code>` even without knowing where it was defined.</p>
<h2>## Final words</h2>
<p>With this, you should have a pretty good understanding of where linking happens on a modern computer. Linking still has a lot of quirks when working with C — after all, we may want some control over what we mark as symbols and how. That will et covered in a future part.</p>
<h2>## Glossary</h2>
<ul>
<li>*<em>interpreter</em>*: the software that executes executables</li>
<li>*<em>standard library</em>*: libraries containing basic functionality that most programmers don't need to think about</li>
</ul>

    
    <footer>
      <p>
        Written on <time datetime="2020-06-01">2020-06-01</time>.
      </p>
    </footer>
    <section>
        <a href="https://comments.dorotac.eu/posts/dynamic_linking">Comments</a>
        <iframe src="https://comments.dorotac.eu/posts/dynamic_linking" style="width: 100%;height: 100vh;" loading="lazy">
            <a href="https://comments.dorotac.eu/posts/dynamic_linking">Comments</a>
        </iframe>
    </section>
  </article>

    </main>
    <section id="global">
        <p><a href="/">dcz's projects</a></p>
        <p>Thoughts on software and society.</p>
        <div>
          <nav>
  <p>posts:</p>
  <ul>
    
        <li><a href="/posts/switch">Domain change</a></li>
    
        <li><a href="/posts/quick_maps">Maps à la carte</a></li>
    
        <li><a href="/posts/holes">Spots on the Sun and worn-out clothes</a></li>
    
        <li><a href="/posts/remote_debugging">Graphical debugging on the Librem 5 using QtCreator</a></li>
    
        <li><a href="/posts/jazda_rust">Jazda: Rust on my bike</a></li>
    
        <li><a href="/posts/why">Why Jazda?</a></li>
    
        <li><a href="/posts/git-botch-email">Git-botch-email</a></li>
    
        <li><a href="/posts/potion">A potion of experience</a></li>
    
        <li><a href="/posts/tabcheology">Browser tab archaeology</a></li>
    
        <li><a href="/posts/rust_maemo">Rust on Maemo</a></li>
    
        <li><a href="/posts/REing a pen">Reverse engineering a pen</a></li>
    
        <li><a href="/posts/in_the_middle">Start in the middle</a></li>
    
        <li><a href="/posts/nerd">When being a nerd paid off</a></li>
    
        <li><a href="/posts/flame_graph">Linux kernel flame graphs</a></li>
    
        <li><a href="/posts/fluwid">Simulating fluwids</a></li>
    
        <li><a href="/posts/blossom">A beautiful blossom of engineering excellence</a></li>
    
        <li><a href="/posts/objective">My objective function will blow it up</a></li>
    
        <li><a href="/posts/flat_earth">Geo in GL part 1: Flattening Earth</a></li>
    
        <li><a href="/posts/input_method">Wayland and input methods</a></li>
    
        <li><a href="/posts/embeddings">Word embedding fun</a></li>
    
        <li><a href="/posts/not_keyboard">It&#39;s not about keyboards</a></li>
    
        <li><a href="/posts/cargo_versions">Why I avoid Cargo: dependency versions</a></li>
    
        <li><a href="/posts/gcc_symbols">From C to exec: Part 3</a></li>
    
        <li><a href="/posts/dynamic_linking">From C to exec: Part 2</a></li>
    
        <li><a href="/posts/linking">From C to exec</a></li>
    
        <li><a href="/posts/brutalism">Brutalist blogging</a></li>
    
  </ul>
</nav>
          <p><a href="/atom.xml">Atom feed (no longer updated)</a></p>
        </div>
        <nav>
          <p>lists:</p>
          <ul>
            <li><a href="/">about me</a></li>
            <li><a href="/forges">my code</a></li>
          </ul>
        </nav>
    </section>
    <footer>
      <p>Articles published under the CC-BY-SA 4.0 license.</p>
    </footer>
  </body>
</html>