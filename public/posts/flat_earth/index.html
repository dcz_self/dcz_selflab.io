<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Geo in GL part 1: Flattening Earth</title>
    <meta http-equiv="Refresh" content="0; URL=https://dorotac.eu/posts/flat_earth" />
    <link rel="stylesheet" href="/style.css" />
  </head>
  <body>
    <section id="notice">
      This site moved to <a href="https://dorotac.eu/posts/flat_earth">dorotac.eu</a>. The version you're seeing now is a preserved copy from April 2023.
    </section>
    <main>

  <article>
    
      <h1># Geo in GL part 1: Flattening Earth</h1>
<p>It's my guilty pleasure to flatten the Earth. To put it on various planar surfaces: pages of paper, scrolls, billboards, screens. Many prominent people attempted that before me: <a href="https://en.wikipedia.org/wiki/Gerardus_Mercator">Mercator</a> and <a href="https://en.wikipedia.org/wiki/Cassini_projection">Cassini</a>, just to name two.</p>
<p>Yes, I'm talking about maps and projections.</p>
<p>This is the first part of the story of how I used OpenGL <a href="https://en.wikipedia.org/wiki/Shader">shaders</a> to project the Earth onto the flat computer screen. Here I introduce the reader to the basics of <a href="https://en.wikipedia.org/wiki/Map_projection">projections</a>, write about the scope of my project, and then choose a projection that fits the scope.</p>
<p>**<strong>Warning</strong>**: if you believe the Earth is already flat, this blog post won't offer anything interesting for you. You might still be interested in the following parts in the series.</p>
<h2>## What's the point?</h2>
<p>I read someone online say shaders are bad for drawing maps: they aren't precise enough to distinguish distances a meter apart! Of course, I don't believe everything online, so I decided to check it myself, while refreshing my OpenGL knowledge, while playing with maps.</p>
<p>Which brings me to the next point.</p>
<h2>## I like maps</h2>
<p>While there are many representations of the real world, maps feel the most real. Some models turn the real into an unrecognizeable mess of abstract concepts. Maps are elegant, because they copy the real thing, just smaller. They aim to be recognizeable and familiar, so making them is always visually satisfying.</p>
<h2>## Distortions</h2>
<p>Maps are not perfect, though. They are not scale models, and they contain some abstract parts: a mountain represented on a map will be as flat as the paper or screen it's on. If it's marked, it will be either a drawing of a mountain, or a squiggle of height lines.</p>
<p>Most maps are also not globes. Our planet is round, and that means a flat map can't capture it accurately. Like a flattened orange peel, the map will get distorted: there will be tears or stretched places. The different ways to flatten the Earth to fit on a map are called *<em>projections</em>*.</p>
<p><img src="ortho.png" alt="3D-like Globe" />
This is also not a globe. It's just its picture! The picture is called *<em>orthographic projection</em>*.</p>
<h2>## The scope</h2>
<p>There are multitudes of projections available. To narrow it down, it helps to define what kind of maps are interesting in this project. With that in mind, the initial version of the program will</p>
<ul>
<li>display only small areas: not more than 100km in size. The smaller the area, the flatter the Earth surface, so distortions are smaller.</li>
<li>Load a GPS track and display it as a squiggle. My GPS tracks are usually only a few hours long, and fit in the 100km size limit.</li>
<li>Display nothing else. I don't want to get carried away and never prove what I set out to prove.</li>
</ul>
<h2>## Projection choices</h2>
<p>The last big decision was to choose a projection.</p>
<h3>### WGS84</h3>
<p>Normally, we're used to seeing geographical coordinates of a point looking like: &quot;*<em>54,5°N 23,7°E</em>*&quot;. This representation seems obvious. We live on a sphere, and so we use <a href="https://en.wikipedia.org/wiki/Spherical_coordinate_system">spherical coordinates</a>. However, the prime meridian, and the precise measurement of the Earth axis, among oteher things, need to be standardized, so this representation is called <a href="https://gisgeography.com/wgs84-world-geodetic-system/">WGS84</a>. Since this is what GPS is using, it would be trivial to just plot the points from track on the screen as they are. However, the results would end up rather unappetizing:</p>
<p><img src="wgs84.png" alt="Map of the world projected using WGS84" /></p>
<p>The distortions are huge! Can you see the grid sizes? Places at the equator are much smaller than at the poles, and the poles are extremely stretched sideways too – if you look at the globe, the grid is composed of wedges close to the poles. This would be a very unpleasant map to look at, even on our small scale.</p>
<h3>### Mercator</h3>
<p>The stretching is easily fixable by applying a varying stretching factor across the vertical position. This gives us the <a href="https://en.wikipedia.org/wiki/Mercator_projection">Mercator projection</a>:</p>
<p><img src="600px-Mercator_projection_Square.JPG" alt="Map of the world projected with the Mercator projection" /> Image copyright <a href="https://commons.wikimedia.org/wiki/User:Strebe">Strebe</a>, CC-BY-SA 3.0 Unported.</p>
<p>This projection is easy to calculate, and it covers most of the world, so it's very popular on the Internet. Most online maps use it, including OpenStreetMap's own <a href="https://osm.org">osm.org</a>. However, it's still rather bad: the stretching factor approaches infinity as we approach the poles, so the map doesn't have a top and bottom edge. Instead it's just cut off somewhere. The other problem is that the areas at the equator are still much smaller than near poles.</p>
<h3>### Something different</h3>
<p>It seems that both of those projections are quite bad at some places. But they are also both quite good at certain places: in this case, at the equator. So why don't we choose the best projection for our data?</p>
<p>That's what paper maps do. Unlike general purpose computer map viewers, paper maps are often limited to a small area on the Earth. They can focus on making this area undistorted, and they can ignore all the misshapen mess that's not printed on the paper. I deliberately chose to limit my program's area of interest to a 100km radius to take advantage of this.</p>
<p>For this, we need a projection that's not general like WGS84, or like the Mercator projection, but instead focussed on the area in question. To look at the North Pole in an undistorted way, we could choose the The North Pole Lambert Azimuthal Equal Area projection:</p>
<p><img src="espg102017.png" alt="Map of the North Pole and surrounding continents" /></p>
<p>Here, I decided to increase my difficulty: I will not make an assumption about where on the Earth my GPS track is. It could be Europe, the Equator, or the Arctic. Instead, I will try to hit the bullseye and make sure that my projection favors the area I'm displaying, no matter what it is. For that, I need to find a family of projections that I can adjust.</p>
<p>Thankfully, people have come up with <a href="https://proj.org/operations/projections/index.html">plenty of different ways of projecting</a>:</p>
<p><a href="https://proj.org/operations/projections/alsk.html"><img src="alsk.png" alt="Alaska projected stereographically" /></a>
<a href="https://proj.org/operations/projections/moll.html"><img src="moll.png" alt="Oval world in Mollweide projection" /></a>
<a href="https://proj.org/operations/projections/wink1.html"><img src="wink1.png" alt="Oval world in Winkel I projection" /></a>
<a href="https://proj.org/operations/projections/vandg.html"><img src="vandg.png" alt="Circle world in van der Grinten (I) projection" /></a>
<a href="https://proj.org/operations/projections/gstmerc.html"><img src="gstmerc.png" alt="World as a crumpled ball of paper in Gauss-Schreiber Transverse Mercator projection" /></a>
<a href="https://proj.org/operations/projections/tobmerc.html"><img src="tobmerc.png" alt="Diamond shaped world in Tobler-Mercator projection" /></a>
<a href="https://proj.org/operations/projections/imw_p.html"><img src="imw_p.png" alt="Cylinder world without poles in International Map of the World Polyconic projection" /></a>
<a href="https://proj.org/operations/projections/ocea.html"><img src="ocea.png" alt="World with North Pole at the bottom edge, and South Pole at the top edge in Oblique Cylindrical Equal Area projection" /></a><br />
Various projections: Alaska, Mollweide, Winkel I, van der Grinten (I), Gauss-Schreiber Transverse Mercator, Tobler-Mercator, International Map of the World Polyconic, Oblique Cylindrical Equal Area. (Images courtesy <a href="https://proj.org">PROJ</a>.)</p>
<p>Some of them are tweakable, to name a few: <a href="https://proj.org/operations/projections/aeqd.html">azimuthal equidistant</a>, <a href="https://proj.org/operations/projections/cass.html">Cassini</a>, <a href="https://proj.org/operations/projections/gnom.html">gnomonic</a>.</p>
<p>Most of them preserve shapes over small areas rather well, so the choice is not that important. Nevertheless, I'm not an expert, so it's possible that I'm underestimating distortions. To stay on the safe side, I choose the azimuthal equidistant, because it promises that distances from the central point can be measured with a ruler. I used it previously with an overlaid kilometer grid and it worked well.</p>
<p>Here's the projection in <a href="https://proj.org/">proj</a> form, so you can apply it in <a href="https://www.qgis.org/en/site/">QGIS</a>. Don't forget to adjust `<code>lat_0</code>` and `<code>lon_0</code>` to your area of interest!</p>
<pre>```
<code>+proj=aeqd +lat_0=54 +lon_0=24 +x_0=0 +y_0=0 +a=6371000 +b=6371000 +units=m +no_defs
</code>```</pre>
<h2>## Next steps</h2>
<p>I have the projection, but I still don't know the math behind it, and don't have any code. Stay tuned for the next part!</p>

    
    <footer>
      <p>
        Written on <time datetime="2020-09-30">2020-09-30</time>.
      </p>
    </footer>
    <section>
        <a href="https://comments.dorotac.eu/posts/flat_earth">Comments</a>
        <iframe src="https://comments.dorotac.eu/posts/flat_earth" style="width: 100%;height: 100vh;" loading="lazy">
            <a href="https://comments.dorotac.eu/posts/flat_earth">Comments</a>
        </iframe>
    </section>
  </article>

    </main>
    <section id="global">
        <p><a href="/">dcz's projects</a></p>
        <p>Thoughts on software and society.</p>
        <div>
          <nav>
  <p>posts:</p>
  <ul>
    
        <li><a href="/posts/switch">Domain change</a></li>
    
        <li><a href="/posts/quick_maps">Maps à la carte</a></li>
    
        <li><a href="/posts/holes">Spots on the Sun and worn-out clothes</a></li>
    
        <li><a href="/posts/remote_debugging">Graphical debugging on the Librem 5 using QtCreator</a></li>
    
        <li><a href="/posts/jazda_rust">Jazda: Rust on my bike</a></li>
    
        <li><a href="/posts/why">Why Jazda?</a></li>
    
        <li><a href="/posts/git-botch-email">Git-botch-email</a></li>
    
        <li><a href="/posts/potion">A potion of experience</a></li>
    
        <li><a href="/posts/tabcheology">Browser tab archaeology</a></li>
    
        <li><a href="/posts/rust_maemo">Rust on Maemo</a></li>
    
        <li><a href="/posts/REing a pen">Reverse engineering a pen</a></li>
    
        <li><a href="/posts/in_the_middle">Start in the middle</a></li>
    
        <li><a href="/posts/nerd">When being a nerd paid off</a></li>
    
        <li><a href="/posts/flame_graph">Linux kernel flame graphs</a></li>
    
        <li><a href="/posts/fluwid">Simulating fluwids</a></li>
    
        <li><a href="/posts/blossom">A beautiful blossom of engineering excellence</a></li>
    
        <li><a href="/posts/objective">My objective function will blow it up</a></li>
    
        <li><a href="/posts/flat_earth">Geo in GL part 1: Flattening Earth</a></li>
    
        <li><a href="/posts/input_method">Wayland and input methods</a></li>
    
        <li><a href="/posts/embeddings">Word embedding fun</a></li>
    
        <li><a href="/posts/not_keyboard">It&#39;s not about keyboards</a></li>
    
        <li><a href="/posts/cargo_versions">Why I avoid Cargo: dependency versions</a></li>
    
        <li><a href="/posts/gcc_symbols">From C to exec: Part 3</a></li>
    
        <li><a href="/posts/dynamic_linking">From C to exec: Part 2</a></li>
    
        <li><a href="/posts/linking">From C to exec</a></li>
    
        <li><a href="/posts/brutalism">Brutalist blogging</a></li>
    
  </ul>
</nav>
          <p><a href="/atom.xml">Atom feed (no longer updated)</a></p>
        </div>
        <nav>
          <p>lists:</p>
          <ul>
            <li><a href="/">about me</a></li>
            <li><a href="/forges">my code</a></li>
          </ul>
        </nav>
    </section>
    <footer>
      <p>Articles published under the CC-BY-SA 4.0 license.</p>
    </footer>
  </body>
</html>