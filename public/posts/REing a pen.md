---
title: "Reverse engineering a pen"
date: "2021-07-15"
---

Reverse engineering a pen
===================

Like a lot of people, I have a favorite pen. I ran across it randomly, but I fell in love with it instantly. It glides across paper with barely any pressure, leaves strong, confident, and uniform streaks, and it's built like a tank to boot. But, unlike most pens that are worthy of appreciation, this pen is really many pens – it's disposable.

![A red ink Uni-Ball Eye pen](pen.png)

While the ink is bright an saturated, and there's a selection of colours, the Uni-Ball Eye is not meant to be refilled. To someone trying to reduce my waste production, using up one perfectly good pen every year is completely unacceptable. Knowing tricks back from the bad days of ink jet printers, I bought a vial of fountain pen ink, a syringe, and I set out to refill the carcass of my most recent pen.

Knowing that this is a blog post about refilling a pen, you might already guess that it wasn't as easy as it sounds.

Sesame open
---------------

Before refilling, I destroyed a previous used up carcass to find best ways to inject liquid into the pen. It took me a while with the scissors – the outer shell is *really* thick – but eventually I found out that the back end has a thinner wall, and a needle did the job.

I also dissected the funny comb-like part near the tip that always looked like it filled with a little bit of ink. It's actually a second ink vessel, whose role I discover while refilling. Surprisingly, this vessel is not connected to the tip. There's a wick going from the tip straight to the big compartment, *inside* the tubular, comb-like second vessel.

Here's a diagram:

![A diagram of a pen. From end to tip, a useless? hole leading to an empty container. A weak wall separating it from the ink container, which contains a thin wick leading all the way to the tip. The ink container ends in a permeable wall, which leads to a container defined by a "comb" structure. The "comb" is a pipe with the wick inside, and many flat rings on its outside. The rings are not reaching the container wall. Two guessed passages connect the end of this container with two "nostrils" near the tip.](diagram%20annotated.svg)

(The walls are much thicker in reality.)

Shooting it up
-----------------

For the first refill, I simply poked a hole in the back of the pen, placed the conjoined pen and syringe horizontally, and squirted in a few drops of the liquid. The wick became flooded with ink, and I could write again! But… all the ink slowly seeped into the comb container, leaving the wick dry after a minute. Even worse, two "nostrils" near the tip readily released ink as long as the comb was full. What's going on?

![Comb compartment](comb.png) This comb container on a new pen is already partially filled. The red streaks are ink.

An obvious culprit is gravity, but the ink wasn't flowing all at once, so there must be more to it.

The comb shape drew my attention. Being composed mostly of walls, it gives the liquid plenty of tight spaces to which it can stick. Did the manufacturer intend to make use of the capillary effect? It would help liquid flow into the comb.

There was the open question of whether the pen was originally airtight, but adding pressure into considerations would make the problem too complicated, so I followed the next obvious lead.

Capillary effect
------------------

If I could prevent the comb compartment from filling up, ink would stay in the main vessel, and I would write indefinitely. The only difference, apart from the opened end, between my refill and the original, was the ink used. Perhaps the original ink was tuned specifically for this pen design. Hoping to alter the liquid to be less eager to climb up the comb, I tried a couple mixes with glycerin and water.

Nosebleed
------------

I tried those tests also with the syringe hole plugged. That's when something unusual happened: the nostrils produced bubbles of air, like a kid who dropped an ice cream. Clearly, that was not just gravity's fault: air doesn't flow out under its own weight.

The story of fountain pens leaking inside airplanes came to my mind. Perhaps this pen had similar considerations?

Air pressure
--------------

I had reasons to believe that yes, they do. First off, why would a pen need "nostrils" connecting to the ink compartment, if not to equalize pressure? Coupled with the comb compartment, this made sense. When the pen is in an upright position, ink flows towards the tip, and there's a pocket of air near the butt. That air would expand when surrounding air pressure drops, and push out ink from the main vessel. The comb area would contain the excess, and the comb structure itself would prevent it from flowing out.

Every time I tried to use the pen in the morning, it cried me tears of ink, flourishing my handwriting in a special way. Pressure is the hint. Mornings are cold. My hands are warm. Gases expand with temperature. Air pushes out ink. It all makes sense!

Flawed methodology
------------------------

Sadly, my tests so far were all flawed: I never allowed the comb to dry up so far, and if the pressure theory was right, then the pen could work correctly only when the comb had plenty of empty space. I came up with a new procedure:

- use the pen until all the ink was released – via the tip or the nostrils
- carefully plug the injection point.

If my theory was right, then the following would happen:

- the liquid would have enough surface tension to not flood the comb after a few hours
- the wick would stay submerged in ink, and never dry up
- writing on cold mornings would make the comb fill up a little
- but it would not be enough to fill up the comb and cause spills.

Experiments
---------------

![A red ink blot](blot.png) My daily companion when researching this article: the ink blot.

The plan didn't work out flawlessly, and every attempt was about as fun as watching ink dry. That's because using up all ink took up ages, and the ink blots added a special flourish to my handwriting. I made several failures to plug the injection point: it turns out that just sticking a needle in there is not enough, and ink flows out the back. What's good enough is glueing it up using some plastic foil and liquid latex.

![The end of a pen plugged with a piece of paper, all soaked with red.](plug.png)

While that works to keep the ink out the back end, it turns out that the ink still flows out the front! That invalidates my theory. Or does it?

I didn't want to seal the pen irreversibly in case that I needed to adjust something, so the end was probably letting air seep in a little. I left the pen overnight in different positions, and, after watching the ink in the comb change, concluded that gravity was responsible for the failure this time.

Without any better ideas, I went ahead and mixed in some glycerin into the ink vessel, which seems to have stopped the outflow. Success!

Inkblots
----------

That last experiment proved the minimal changes needed for the Uni-Ball Eye to be reuseable:

- use ink with about 10% glycerin
- refill from the back while the comb compartment is not full
- plug in the refill hole afterwards.

The comb compartment is probably a way to avoid leaks due to pressure.

![Two pens next to similar squiggles of red ink. The bottom is broader and brighter than the top one.](comparison.png) The top pen in this picture is the refilled one. In reality, both inks look darker.

The ink I used is not as intense as the original one, and it doesn't flow so easily, but on the other hand, it's a bit thinner, and still flows decently enough. It can work as a cheap alternative to a new pen, which normally costs about 3 EUR per piece.

Lessons learned
-------------------

Turns out that there's a good deal of thought and complexity embedded in every day objects. Physical objects are take more effort to understand than as software. You need to control your environment, retrying the experiment can take a lot of time, and you may get yourself dirty in the middle. None of this will be a surprise to an experimental scientist, of course.

It's still a joy to explore, and the results are going to be a fair bit easier to explain to a random human – the outcome is satisfyingly concrete!