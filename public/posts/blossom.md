---
title: "A beautiful blossom of engineering excellence"
date: "2021-04-02"
---

A beautiful blossom of engineering excellence
==========================

Have you ever created something perfect? Simple and functional, robust, and with a core that needs no further adjustment?

I have, recently. I named it [odwal](https://gitlab.com/dcz_self/odwal). I would have chosen a more positive name if I had known the peace of mind it bestowed upon me in the last several months.

*Odwal* is perfectly underhanded. It doesn't look special, and I use it only to decrypt and mount my offline backup drives. But it takes care of all the tedium of the process: it deals with failures, and it stops the drives after I'm done.

It was not entirely my invention. The core perfectness comes from the idea of a [dependency graph](https://en.wikipedia.org/wiki/Dependency_graph), known to many from the [make](https://www.gnu.org/software/make/) tool. Whereas *make* is only concerned with creating resources, *odwal* takes care of releasing them in the right order too.

This snippet takes care of mounting and unmounting my backup:

```
(defstep backup_hdd
  (BlockStorage
    args (id "ata-redacted") ; filename from /dev/disk/by-id
  )
)

(defstep decrypted
    (EncryptedBlock
        args (
            path parent.0.path
            name "full1"
         )
         parents (backup_hdd)
    )
)

(defstep full_partitions
    (Partitions
       args (path parent.0.path)
       parents (decrypted)
    )
)

(Mount
    args (
        path "/dev/mapper/full1p1"
        destination "/backups/full"
    )
    parents (full_partitions)
)
```

First, I connect the USB enclosure to my computer. Then, I run *odwal*:

```
# python3 -m odwal ./odwal.to hold Mount:/dev/mapper/full1p1
[here odwal checks for the drive]
Enter passphrase for /dev/sdc:
[here odwal mounts the partition]
Press enter to tear down
```

*Odwal* asks me for the decryption password while preparing the mount, and then it gracefully waits until I no longer need it. Once I'm finished, I press enter, and *odwal* spins down the drive.

Even if I plug in the wrong drive, I don't have to worry. *Odwal* won't touch it. Even if I mess up the configuration, no harm is done. All I need to do is run `odwal clear`, and the world is good again.

The relief of using this compared to a bunch of shell scripts is impossible to describe. And it's only 400 lines of Python!

I hope one day I can bring my backup process halfway to the level of delight *odwal* represents.