---
title: "It's not about keyboards"
date: "2020-07-03"
---

It's not about keyboards
=================

My work on [Squeekboard](https://source.puri.sm/Librem5/squeekboard/) and the corresponding Wayland protocols is open and public. That lets people come and contribute, and give feedback. Those people come with different expectations about the result of my work, and there's one that looks enticing but doesn't really correspond with what my work is about. It causes lots of confusion, so I would like to dispel it.

Squeekboard's goal is not to be a *keyboard*.

Instead, I designed Squeekboard and the pieces around it to support *text composition* first and foremost. While a *keyboard* is one method of composing text, it usually does other things as well:

- playing musical notes
- turning things on and off
- shooting virtual monsters
- moving within virtual space
- instructing the computer to do arbitrary actions.

Squeekboard doesn't want to do any of those things, unless they help composing text. To understand why, let's look at the basic properties of keyboards and Squeekboard. Keyboards

- have labels which don't change
- have keys which stay in place and retain their shape forever
- have keys which sense force
- have keys which provide precise tactile feedback.

Meanwhile, Squeekboard:

- occupies a changeable area of the screen
- has pixels which change color but not shape
- has touch points which overlap the display
- has touch points that capture "pressure".

As you can see, they are very different beasts, with different constraints and abilities

It's just an app!
------------------

Squeekboard has some ability to emulate the keyboard buttons and register touches, so in principle it could share the goals of a keyboard. The key to understanding why this is not a useful useful goal is to realize that all of Squeekboard's unique abilities are the same as of any other graphical program.

That gives us a new constraint: Squeekboard should not duplicate the functionality of the application it controls.

A physical keyboard has it easy: touch feedback and less hand movement always adds value. But what can an on-screen one offer? Let's think about the interactions the user has with an application.

The user might want to close or resize the application. That's easy, the compositor takes care of that. The user might want to issue an application-specific action, like rendering a scene, or moving the camera. Arguably, this is best handled by the application, which can present the relevant information. Commonly encountered actions like saving the file, copying or opening preferences are usually supported in a built-in way too. What does that leave us?

Text input.

Few applications give user an interface to enter text directly, making it the obvious responsibility of Squeekboard as an input method.

Extra powers
----------------

Dropping the preconception that an on-screen input method is a keyboard opens new possibilities for text input. Changing button shapes and numbers, presenting useful information in labels, offering corrections and suggestions, gestures, handwriting recognition, embedded input methods, and sending text directly to the application (not as button presses) are not things that can be done with just a matrix of buttons.

While Squeekboard doesn't support many of those features, the groundwork for most of them is included in the Wayland protocols developed alongside it.

Looking past the historically proven idea of a keyboard opens a different view on how touch-based human-computer interfaces should work, and Squeekboard is there to make best use of the new possibilities.