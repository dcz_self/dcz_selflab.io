---
title: "When being a nerd paid off"
date: "2021-07-06"
---

When being a nerd paid off
===================

A few days ago, my knowledge of obscure computing facts surprisingly saved the day! Surprisingly, because sticking to garden variety best principles would have left me worrying. What was the deal? It's about a deleted photo on my camera.

* * *

It was a sunny day, so I copied photos off my camera onto the computer, and set out with a sense of journey and a clean storage. Or so I thought: when I stopped to take a photo, my camera complained about full storage, so I selected "delete all photos". While the screen showed "erasing", I realized in horror: I took a cute picture of a dilapidated wagon already! And I'm not going back there again to take another one.

Common advice in those situations says: don't use the storage medium under no circumstances, and attempt recovery on a computer. Then you might be lucky and keep your deleted data from being overwritten.

This advice is right, but the trip was just beginning, and I was not ready to stop taking photos. Thankfully, my nerdy sense told me that I have a very good chance to get the picture back even if I take more photos – as long as I don't go crazy with them. That's due to some secrets that I learned about technology:

- computers (and therefore digital cameras) are made by humans, and humans like simplicity,
- modern storage is logically structured a bit like a cassette tape (or a photo film),
- "deleted" data does not actually get cleared until it's overwritten by new data.

You might be surprised to learn about the similarity of storage to tape, but it's there on hard drives, actual tape drives, SD cards, and SSDs. The structure exposed to the programmer is of a long string of small pieces of data (sectors), each of which has a number, starting from 0, almost like houses on the street.

![A drawing of numbered houses on Sector Street, numbers 1 to 5](sectors.svg)

Accessing sectors (by writing or reading) is simplest when it's done in order. We're only humans, and why use an elaborate scheme if we can just move from one to the next? And so, the reading head slides steadily on the tape, the film in a camera moves by one frame after each photo, the postman visits houses in order. And it's not just out of convenience: it's clearly the fastest when there's a moving physical object involved. Surprisingly, writing in order is even the fastest way to [write](https://www.anandtech.com/show/2738/25) data on SSDs, which have no moving parts!

Append only
---------------

This must be coupled with an observation: cameras – or at least my camera – only add new pictures onto the SD card (our "tape"), and when deletions happen, the entire contents of the card are marked as deleted. Just like in a film camera: once a photo is taken, it's taken. We can replace the entire tape, but we don't replace a single picture with a blank.

This makes life easier, because imagine if we had a tape where we could erase already taken pictures. At first it's okay: we take 30 pics, and we hit the end. Then we erase pictures number 7, 2, 17, and 22. To take another picture, we would have to fiddle with the tape to find an empty spot, then rewind it to the right position (make sure it's not overlapping!), and do it again until the tape is full again. That's no fun. It's easier if pictures come one after another.

And while I don't have any evidence, I assume that's how my digital camera works, too. It's not a general purpose computer, so it doesn't have to deal with a lot of deletion. I can safely assume that the picture of the wagon (which was the last picture on my SD card before it filled up) will not occupy the same sectors as the first 10 pictures I take after the card is erased, and that it will not get overwritten.

With a peace of mind (and ready to be proven wrong), I continued to take photos of the trip. When back home, I fired up *testdisk*. Lo and behold, I recovered the picture!

![A dilapidated wooden wagon with one wheel modern and one missing](wagon.jpg)

The real world
-----------------

Okay, maybe I just got lucky. In reality, there's one thing that changes size. For SD cards, it's usually the File Allocation Table. It contains the numbers of sectors which contain pieces of each file, along with the names, and a bunch of extra info. As enough files and directories get added, it probably changes in size.

Another complication is the mysterious "PRIVATE" directory. I presume this contains only hibernation data to make the camera start faster, and I'm not seeing any reason for it to change size at all (what kind of generated data does a camera need anyway?).

In the end, what matters is not exactly that sectors get written from 0 up. What matters is that there is a defined order of writing which doesn't change between full card erasures. That's much more difficult to verify.

FAT32
--------

FAT32 doesn't actually mark sectors as "deleted". It marks the entire file as deleted, without permanently erasing any of its information until it's needed. Testdisk knows this, and it makes recovery a breeze. The entire operation took 5 minutes.

CAUTION
------------

This is all based on high level guessing, so don't rely on this. I made peace with losing the deleted picture when I decided to keep shooting new ones. Basic advice is still best: if you accidentally delete something important, immediately stop using the storage, **do not mount the file system**, and make a *block level* copy. And why don't you restore it from a backup anyway?