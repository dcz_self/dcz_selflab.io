<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>A potion of experience</title>
    <meta http-equiv="Refresh" content="0; URL=https://dorotac.eu/posts/potion" />
    <link rel="stylesheet" href="/style.css" />
  </head>
  <body>
    <section id="notice">
      This site moved to <a href="https://dorotac.eu/posts/potion">dorotac.eu</a>. The version you're seeing now is a preserved copy from April 2023.
    </section>
    <main>

  <article>
    
      <h1># A potion of experience</h1>
<p>You may notice that this blog has just been enriched, especially if you're following my <a href="https://fosstodon.org/@dcz">Mastodon account</a>. See that thing all the way past the article? It's a comments section. Cause I want to have a conversation with my readers, rather than keep shouting into the void.</p>
<p>You can surely see it's pretty basic.</p>
<p>I made it myself. See, I had my eye for a while on this elixir that would give me some network development experience, and I decided to use it. I had to supplement it with some extra stuff to get anything more than *<em>just</em>* experience, but I'll talk about that later.</p>
<h2>## Glug glug</h2>
<p>It's not a mystery: it's just <a href="https://elixir-lang.org/">Elixir</a>. It's a language running on the BEAM virtual machine, and taking advantage of the Erlang/OTP runtime. OTP has fascinated me for a long time, with its &quot;<a href="https://lwn.net/Articles/191059/">crash-only</a>&quot; approach, a concurrent take on the <a href="https://www.brianstorti.com/the-actor-model/">Actor model</a>, and aggressive immutability.</p>
<p>In short, it's a programming paradigm outside of the orthodoxy I've known. And what better source of new insights is there than an unusual point of view?</p>
<p>Disclaimer: I'm still rather new to Elixir, so if I mess up some terminology, let me know… in the comments ;)</p>
<h2>## Level up</h2>
<p>The feature of Elixir that I liked the most comes from Erlang/OTP: it's the actor model. Your program gets split into… uh, &quot;applications&quot;, which operate as independent threads, and can exchange messages. For example, the web module sends a message to the email module to let me know I should approve a new comment, and doesn't even wait for an answer.</p>
<p>If the email sender crashes, the web service keeps churning on, and only the email component gets restarted. And I don't even need to care about the restart logic!</p>
<p>That's already cool, but what if I want extra logic in the email sender? I don't want to be spammed by a lot of emails when my blog reaches the front page of Hacker News, but rather have the email module wait for 5 minutes between notifications.</p>
<p>Immutability then changes the rules of the game: you can't just save the time of the last email in a local variable, and handle messages in a loop. There's a kind of a local database for each application, where data can be explicitly stored. One thing comes to another, and I implemented a state machine to deal with the notifications.</p>
<p>And that's where I levelled up: I stated using state machines, which exchange messages to cause transitions. It's an excellent organization to debug, because you can serialize the state of your module, and see how exactly external events cause changes in the state. If you add a little more effort to turn side effects into more messages, you can perform &quot;in vitro&quot; state transformations as part of your test suite.</p>
<h2>## Bitter taste</h2>
<p>Elixir is not all good though. The one especially bittersweet part is how much metaprogramming it allows. The basic syntax ends up being simple (or so I'm told), but once you actually start using Elixir, expect surprising syntax constructs. Here are a couple of examples that keep confusing me.</p>
<p>Most statements are contained between `<code>do</code>` and `<code>end</code>`, <a href="https://elixir-lang.org/getting-started/case-cond-and-if.html">like this</a>:</p>
<pre>```
<code>if true do
  x = x + 1
  y = y + 1
end
</code>```</pre>
<p>But not stuff inside branches of a `<code>case</code>` expression:</p>
<pre>```
<code>case foo do
    true -&gt;
      x = x + 1
      y = y + 1
    false -&gt;
      x = x
end
</code>```</pre>
<p>I feel uneasy each time I write this. What's the delimiter marking the end of the `<code>true</code>` branch? Note that indentation doesn't matter here.</p>
<hr />
<p>Another annoying property of some Elixir libraries is spooky action at a distance, where the reasons for doing something are implicit and hidden away behind layers of abstraction. Here's an excerpt from the <a href="https://elixirschool.com/en/lessons/misc/plug">Plug library tutorial</a>:</p>
<pre>```
<code>defmodule Example.Router do
  use Plug.Router

  alias Example.Plug.VerifyRequest

  plug Plug.Parsers, parsers: [:urlencoded, :multipart]
  plug VerifyRequest, fields: [&quot;content&quot;, &quot;mimetype&quot;], paths: [&quot;/upload&quot;]
  plug :match
  plug :dispatch

  get &quot;/&quot; do
    send_resp(conn, 200, &quot;Welcome&quot;)
  end

  get &quot;/upload&quot; do
    send_resp(conn, 201, &quot;Uploaded&quot;)
  end

  match _ do
    send_resp(conn, 404, &quot;Oops!&quot;)
  end
end
</code>```</pre>
<p>Take a look at the `<code>plug</code>` lines. They will take care of encoding and verification. It's nifty because you don't have to worry, just slap those in. It's confusing because if you're a newbie and want to stay near the basics, those constitute a barrier. It's opaque, because there doesn't seem to be a syntactical opening to declare some calls to be affected by those filters.</p>
<p>In the end, I avoided the problem by never using such clever tricks, at the cost of having less learning material (not that the material with tricks taught me anything).</p>
<hr />
<p>In the end, it comes out as a mostly positive coding experience, and I'll choose Elixir over Django in the future.</p>
<p>But coding the web app is not all.</p>
<h2>## Hangover</h2>
<p>After coding, I had to deploy it on my server somehow, or else experience is all I get. This grew to be a full half of the experience, and a half I'd rather do without.</p>
<p>Let me start with Ansible.</p>
<p>I don't like it.</p>
<p>It disappoints me in one crucial area: it does not compose. I can't easily create Ansible instructions to deploy <a href="https://gitlab.com/dcz_self/beng">Beng</a> on a pristine system for developers, and then reuse the same instructions to deploy it on my infra alongside other pages. Isn't that what programming languages are good at? Executing batches of instructions differing by parameters? Perhaps I'm too much of an Ansible noob, but I haven't found the necessary flexibility there.</p>
<p>So I wrote a couple idempotent shell scripts to replace Ansible.</p>
<p>**<strong>INTERMISSION</strong>**</p>
<p>Back to Elixir - it turns out that the binaries need a certain version of the Erlang runtime to be present on the destination system. CentOS 7 didn't have the same version as my development machine. I couldn't build them in a CentOS container either.</p>
<p>Long story short, I gave up on CentOS and went with Nix to build the comments app.</p>
<p>The upside is that if I want, NixOS can take over a lot of what I needed Ansible for: building the software, configuring it, installing dependencies. The downside is, when I came back to the app 6 months later, my Nix package utterly and completely doesn't build. I'm not so sure about employing Nix for server config duties now.</p>
<p>**<strong>END INTERMISSION</strong>**</p>
<p>My shell scripts were still needed to move the data and configs from the development machine to the server. Step by step, they grew into something bigger. Something monstrous. Something like… Ansible? But with some neat features that Ansible doesn't have:</p>
<ul>
<li>my system can deploy stuff in Docker containers or SSH hosts. Ansible needs SSH to function.</li>
<li>My system supports actual conditionals.</li>
</ul>
<p>Sadly, it doesn't compose much better. I still have some hardcoded things I don't want to share with the world, so it will remain unpublished for now.</p>
<h2>## Aftermath</h2>
<p><a href="https://gitlab.com/dcz_self/beng">The Elixir app</a> and the deployment sweat were a good lesson in humility. Originally, I estimated the whole thing to take a week. It took two weeks of intense work across several months. Last week I estimated the deployment portion to take an evening. That alone took a week. But now I have something to show for it, and the lessons I learned from Elixir levelled me up as a programmer, so… I guess it was worth it.</p>

    
    <footer>
      <p>
        Written on <time datetime="2022-03-04">2022-03-04</time>.
      </p>
    </footer>
    <section>
        <a href="https://comments.dorotac.eu/posts/potion">Comments</a>
        <iframe src="https://comments.dorotac.eu/posts/potion" style="width: 100%;height: 100vh;" loading="lazy">
            <a href="https://comments.dorotac.eu/posts/potion">Comments</a>
        </iframe>
    </section>
  </article>

    </main>
    <section id="global">
        <p><a href="/">dcz's projects</a></p>
        <p>Thoughts on software and society.</p>
        <div>
          <nav>
  <p>posts:</p>
  <ul>
    
        <li><a href="/posts/switch">Domain change</a></li>
    
        <li><a href="/posts/quick_maps">Maps à la carte</a></li>
    
        <li><a href="/posts/holes">Spots on the Sun and worn-out clothes</a></li>
    
        <li><a href="/posts/remote_debugging">Graphical debugging on the Librem 5 using QtCreator</a></li>
    
        <li><a href="/posts/jazda_rust">Jazda: Rust on my bike</a></li>
    
        <li><a href="/posts/why">Why Jazda?</a></li>
    
        <li><a href="/posts/git-botch-email">Git-botch-email</a></li>
    
        <li><a href="/posts/potion">A potion of experience</a></li>
    
        <li><a href="/posts/tabcheology">Browser tab archaeology</a></li>
    
        <li><a href="/posts/rust_maemo">Rust on Maemo</a></li>
    
        <li><a href="/posts/REing a pen">Reverse engineering a pen</a></li>
    
        <li><a href="/posts/in_the_middle">Start in the middle</a></li>
    
        <li><a href="/posts/nerd">When being a nerd paid off</a></li>
    
        <li><a href="/posts/flame_graph">Linux kernel flame graphs</a></li>
    
        <li><a href="/posts/fluwid">Simulating fluwids</a></li>
    
        <li><a href="/posts/blossom">A beautiful blossom of engineering excellence</a></li>
    
        <li><a href="/posts/objective">My objective function will blow it up</a></li>
    
        <li><a href="/posts/flat_earth">Geo in GL part 1: Flattening Earth</a></li>
    
        <li><a href="/posts/input_method">Wayland and input methods</a></li>
    
        <li><a href="/posts/embeddings">Word embedding fun</a></li>
    
        <li><a href="/posts/not_keyboard">It&#39;s not about keyboards</a></li>
    
        <li><a href="/posts/cargo_versions">Why I avoid Cargo: dependency versions</a></li>
    
        <li><a href="/posts/gcc_symbols">From C to exec: Part 3</a></li>
    
        <li><a href="/posts/dynamic_linking">From C to exec: Part 2</a></li>
    
        <li><a href="/posts/linking">From C to exec</a></li>
    
        <li><a href="/posts/brutalism">Brutalist blogging</a></li>
    
  </ul>
</nav>
          <p><a href="/atom.xml">Atom feed (no longer updated)</a></p>
        </div>
        <nav>
          <p>lists:</p>
          <ul>
            <li><a href="/">about me</a></li>
            <li><a href="/forges">my code</a></li>
          </ul>
        </nav>
    </section>
    <footer>
      <p>Articles published under the CC-BY-SA 4.0 license.</p>
    </footer>
  </body>
</html>