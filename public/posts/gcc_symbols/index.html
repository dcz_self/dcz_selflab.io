<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>From C to exec: Part 3</title>
    <meta http-equiv="Refresh" content="0; URL=https://dorotac.eu/posts/gcc_symbols" />
    <link rel="stylesheet" href="/style.css" />
  </head>
  <body>
    <section id="notice">
      This site moved to <a href="https://dorotac.eu/posts/gcc_symbols">dorotac.eu</a>. The version you're seeing now is a preserved copy from April 2023.
    </section>
    <main>

  <article>
    
      <h1># From C to exec: Part 3</h1>
<p>In <a href="/posts/linking/">Previous</a> <a href="/posts/dynamic_linking/">parts</a>, we followed a story of a piece of code transformed into a linked executable. The center stage was taken by symbols, around which the entire process of linking happens. They are the crucial component that lets us put programs fro multiple pieces. However, we watched what happens to symbols after they are there, but we haven't seen how to control their existence, and how to solve problems we may encounter.</p>
<p>The third part focuses on practical problem solving related to symbols and C.</p>
<h2>## Emitting symbols in C</h2>
<p>You might remember the term &quot;translation unit&quot;. It's just a more convenient name for &quot;a thing that is compiled together&quot;, the bunch of source files that result in a single object file. This simple definition will be used to refer to the sources as opposed to the object file.</p>
<p>C does not support namespaces, so whenever you define a function under the same name in a different context (e.g. a logging function, an error handler or a container operation), they truly take the same name. This can lead to trouble. Let's introduce a new set of files: `<code>floats.c</code>`:</p>
<pre>```
<code>#include &lt;stdio.h&gt;
void print_number(float n) {
  printf(&quot;Float %f\n&quot;, n);
}

void multiply_by_tau(int number) {
  print_number(6.68 * number);
}
</code>```</pre>
<p>Then `<code>float.h</code>`:</p>
<pre>```
<code>void multiply_by_tau(int number);
</code>```</pre>
<p>Finally, `<code>main.c</code>`:</p>
<pre>```
<code>#include &lt;stdio.h&gt;
#include &quot;float.h&quot;

void print_number(int number) {
  printf(&quot;Integer %d\n&quot;, number);
}

void power_of_two(int number) {
  print_number(2 &lt;&lt; number);
}

int main(void) {
  multiply_by_tau(100);
  power_of_two(10);
  return 0;
}
</code>```</pre>
<p>In this example, we end up having two different functions called `<code>print_number</code>`. One is in the translation unit with `<code>main.c</code>`, the other in `<code>float.c</code>`. They are only called from the relevant translation unit, so we should be fine, right?</p>
<pre>```
<code># gcc -c main.c -o main.o
# gcc -c float.c -o float.o
# nm main.o | grep print_number
0000000000000000 T print_number
# nm float.o | grep print_number
0000000000000000 T print_number
# gcc float.o main.o -o main
/usr/bin/ld: main.o: in function `print_number':
main.c:(.text+0x0): multiple definition of `print_number'; float.o:float.c:(.text+0x0): first defined here
collect2: error: ld returned 1 exit status
</code>```</pre>
<p>Not really. We emitted a symbol in both of them, and the linker doesn't know which one to choose. Oops. We have to tell the compiler explicitly that each of those functions will only ever be called from the same translation unit. C defines a keyword `<code>static</code>` for this purpose. Let's check if it works. `<code>float.c</code>` becomes:</p>
<pre>```
<code>#include &lt;stdio.h&gt;
static void print_number(float n) {
  printf(&quot;Float %f\n&quot;, n);
}

void multiply_by_tau(int number) {
  print_number(6.68 * number);
}
</code>```</pre>
<p>And `<code>main.c</code>` now looks like this:</p>
<pre>```
<code>#include &lt;stdio.h&gt;
#include &quot;float.h&quot;

static void print_number(int number) {
  printf(&quot;Integer %d\n&quot;, number);
}

void power_of_two(int number) {
  print_number(2 &lt;&lt; number);
}

int main(void) {
  multiply_by_tau(100);
  power_of_two(10);
  return 0;
}
</code>```</pre>
<p>And the compilation:</p>
<pre>```
<code># gcc -c float.c -o float.o
# gcc -c main.c -o main.o
# gcc float.o main.o -o main
# ./main
Float 668.000000
Integer 2048
# nm main.o
0000000000000045 T main
                 U multiply_by_tau
0000000000000022 T power_of_two
                 U printf
0000000000000000 t print_number
# nm float.o
0000000000000024 T multiply_by_tau
                 U printf
0000000000000000 t print_number
</code>```</pre>
<p>Success! We turned`<code>print_number</code>` into a <a href="https://www.technovelty.org/code/why-symbol-visibility-is-good.html">*<em>local symbol</em>*</a>, (`<code>t</code>` is lowercase) which is ignored during linking.</p>
<h2>## Keywords</h2>
<p>C defines keywords for controlling linkage type. Those are:</p>
<ul>
<li>`<code>extern</code>` for external linkage, i.e. emitting a symbol</li>
<li>`<code>static</code>` for internal linkage, i.e. limiting usage to the current translation unit</li>
<li>no keyword defaults to `<code>extern</code>`</li>
</ul>
<p>However, functions aren't the only items that can be assigned symbols. While any kind of memory area can get a symbol, global variables are the other most common usage. Let's see it all in action in `<code>linkage.c</code>`:</p>
<pre>```
<code>int v_def = 0;
extern int v_ext = 0;
static int v_stat = 0;

void f_def(void) {};
extern void f_ext(void) {};
static void f_stat(void) {};
</code>```</pre>
<p>Looking at the symbols shows us:</p>
<pre>```
<code># gcc -c linkage.c -o linkage.o
linkage.c:2:12: warning: ‘v_ext’ initialized and declared ‘extern’
    2 | extern int v_ext = 0;
      |            ^~~~~
# nm linkage.o
0000000000000000 T f_def
0000000000000007 T f_ext
000000000000000e t f_stat
0000000000000000 B v_def
0000000000000004 B v_ext
0000000000000008 b v_stat
</code>```</pre>
<p>As expected, the default is to emit a symbol, and `<code>static</code>` makes the symbol local. But there's an unexpected warning from the compiler, suggesting that `<code>extern</code>` on a variable does more than just emit a symbol for it. Let's try to apply compiler's advice and not assign any value in`<code>variable.c</code>`:</p>
<pre>```
<code>extern int v_ext;
</code>```</pre>
<p>Looking at symbols:</p>
<pre>```
<code># gcc -c variable.c -o variable.o
# nm variable.o
#
</code>```</pre>
<p>…nothing? Let's try something else in `<code>variable.c</code>`:</p>
<pre>```
<code>extern int v_ext;

void use_variable(void) {
  v_ext = 0;
}
</code>```</pre>
<p>Symbols:</p>
<pre>```
<code># gcc -c variable.c -o variable.o
# nm variable.o
0000000000000000 T use_variable
                 U v_ext
</code>```</pre>
<p>There it is, undefined! Without the body of the variable, the compiler took `<code>extern</code>` to mean &quot;not defined here&quot; instead of &quot;emit a symbol&quot;. No wonder that we didn't see its trace when we didn't try to make use of it. This is similar to *<em>function prototypes</em>* we covered earlier.</p>
<p>Notice that `<code>U</code>` printed by the `<code>nm</code>` program does not mean &quot;symbol of the undefined kind present&quot;. Instead, it means &quot;the relocation table calls for this symbol, but we don't have it&quot;. Here, `<code>nm</code>` mixes up the symbol table and the relocations table, which can be confusing.</p>
<h2>## Using variables</h2>
<p>Variables with the `<code>extern</code>` symbol may sound confusing at first, but they work the same way functions do. Let's take an example of three files. First is `<code>main_var.c</code>`</p>
<pre>```
<code>#include &lt;stdio.h&gt;
#include &quot;var.h&quot;

int main(void) {
  set_var(10);
  printf(&quot;var is %d\n&quot;, var);
}
</code>```</pre>
<p>The file `<code>var.h</code>`:</p>
<pre>```
<code>extern int var;
extern void set_var(int v);
</code>```</pre>
<p>And finally, `<code>var.c</code>`:</p>
<pre>```
<code>int var = 5;
void set_var(int v) {
  var = v;
}
</code>```</pre>
<p>Compiling them shows us that both the `<code>set_var</code>` function and the `<code>var</code>` variable are needed by `<code>main_var.c</code>`, and provided by `<code>var.c</code>`:</p>
<pre>```
<code># gcc -c main_var.c -o main_var.o
# gcc -c var.c -o var.o
# gcc main_var.o var.o -o main_var
# ./main_var 
var is 10
# nm main_var.o
0000000000000000 T main
                 U printf
                 U set_var
                 U var
# nm var.o
0000000000000000 T set_var
0000000000000000 D var
</code>```</pre>
<p>As you can see, we managed to modify a variable from a different file using a function from a different file, and then read it out directly. This mechanism is commonly used to give programs access to memory managed by a library.</p>
<h2>## Header file trouble</h2>
<p>There is a situation is when a function clashes with itself. It sounds silly, but this can happen if the same function creates a symbol in multiple translation units.</p>
<p>This usually happens when a function lives inside a header of a library we're using. Headers are not actually restricted to holding function prototypes, but instead they are fully fledged pieces of C code, verbosely included in the source file. Let's create `<code>source.c</code>`:</p>
<pre>```
<code>const int before;
#include &quot;header.h&quot;
const int after;
</code>```</pre>
<p>And `<code>header.h</code>`:</p>
<pre>```
<code>const int inside;
</code>```</pre>
<p>After running the C preprocessor, which happens as the first compilation step, we get:</p>
<pre>```
<code>$ gcc -E source.c
# 1 &quot;source.c&quot;
# 1 &quot;&lt;built-in&gt;&quot;
# 1 &quot;&lt;command-line&gt;&quot;
# 31 &quot;&lt;command-line&gt;&quot;
# 1 &quot;/usr/include/stdc-predef.h&quot; 1 3 4
# 32 &quot;&lt;command-line&gt;&quot; 2
# 1 &quot;source.c&quot;
const int before;
# 1 &quot;header.h&quot; 1
const int inside;
# 3 &quot;source.c&quot; 2
const int after;
</code>```</pre>
<p>We can see here that the text from the header was indeed included in place of the `<code>#include</code>` directive.</p>
<p>Now, why would anyone actually *<em>want</em>* to place a function in the header?</p>
<p>As you can see, the inclusion of a full function inside header means that it will land in the translation unit of the *<em>calling</em>* file, instead of the *<em>providing</em>* file, unlike in our library examples. Here we don't even *<em>have</em>* a separate &quot;providing&quot; file. We can see that our single translation unit contains what the header provided:</p>
<pre>```
<code># gcc -c source.c -o source.o
# nm source.o
0000000000000004 C after
0000000000000004 C before
0000000000000004 C inside
</code>```</pre>
<p>The most obvious purpose for placing a function in the same translation unit as the caller is optimization. Making a function call is slow. In broad strokes, the compiled code usually saves a generic set of working data, as part of issuing a call and restores it as part of returning from it. But this doesn't make sense if our function is relatively simple. Sometimes it's more efficient to copy the contents of the function instead of calling it. The name for that is *<em>inlining</em>*.</p>
<p>A call to a function in a dynamic library can't be inlined automatically. That would require some advanced processing at the time our program is starting. A call to a function in a static library can sometimes be inlined. That is called *<em>link-time optimization</em>*, and it's done at linking time as a sort of additional, slow compilation step.</p>
<p>On the other hand, a call to a function in the same translation unit can be inlined as part of the compilation process even before the linking, and without much additional penalty. If we place the body of our function in the header, this is exactly what we get, regardless of the kind of library we create in our other translation units.</p>
<p>But what does this have to do with a symbol clashing with itself? Let's introduce an example of a library that must store something for bookkeeping, but doesn't want to be a burden for the calling code. Starting with `<code>store.c</code>`:</p>
<pre>```
<code>int calls_made = 0;
</code>```</pre>
<p>Corresponding `<code>store.h</code>`:</p>
<pre>```
<code>extern int calls_made;

void log_call(void) {
  calls_made += 1;
}
</code>```</pre>
<p>Now, `<code>user.c</code>`:</p>
<pre>```
<code>#include &lt;stdio.h&gt;
#include &quot;store.h&quot;

void print_and_bump(void) {
  puts(&quot;Caller print&quot;);
  log_call();
}
</code>```</pre>
<p>Its `<code>user.h</code>`:</p>
<pre>```
<code>void print_and_bump(void);
</code>```</pre>
<p>Finally, `<code>clash.c</code>`:</p>
<pre>```
<code>#include &lt;stdio.h&gt;
#include &quot;store.h&quot;
#include &quot;user.h&quot;

int main(void) {
  puts(&quot;main print&quot;);
  log_call();
  print_and_bump();
  return 0;
}
</code>```</pre>
<p>Let's try compiling and linking this project:</p>
<pre>```
<code># gcc -c store.c -o store.o
# gcc -c user.c -o user.o
# gcc -c clash.c -o clash.o
# gcc store.o user.o clash.o -o clash
/usr/bin/ld: clash.o: in function `log_call':
clash.c:(.text+0x0): multiple definition of `log_call'; user.o:user.c:(.text+0x0): first defined here
collect2: error: ld returned 1 exit status
</code>```</pre>
<p>That's an error! Remember that we placed the `<code>log_call</code>` function into the header file. That means that every translation unit including the header file will get a copy. We can see that this is what happened:</p>
<pre>```
<code># nm store.o | grep log_call
# nm user.o | grep log_call
0000000000000000 T log_call
# nm clash.o | grep log_call
0000000000000000 T log_call
# gcc -E user.c
[...]
# 1 &quot;store.h&quot;
extern int calls_made;

void log_call(void) {
  calls_made += 1;
}
# 3 &quot;user.c&quot; 2
[...]
# gcc -E clash.c
[...]
# 1 &quot;store.h&quot;
extern int calls_made;

void log_call(void) {
  calls_made += 1;
}
# 3 &quot;clash.c&quot; 2
[...]
</code>```</pre>
<p>The solution is straightforward. Let's change `<code>store.h</code>` to show this:</p>
<pre>```
<code>extern int calls_made;

static void log_call(void) {
  calls_made += 1;
}
</code>```</pre>
<p>Linking test:</p>
<pre>```
<code># gcc -c user.c -o user.o
# gcc -c clash.c -o clash.o
# gcc store.o user.o clash.o -o clash
# nm clash.o | grep log_call
0000000000000000 t log_call
# nm user.o | grep log_call
0000000000000000 t log_call
</code>```</pre>
<p>That did it! Our static function's symbol turned local, and stopped being a duplicate.</p>
<h2>## Inline woes</h2>
<p>You might know the C99 keyword `<code>inline</code>` already. It's meant to be a suggestion to the compiler that the function should be copied instead of called. It turns out that it affects linking.</p>
<p>Taking the example from above and modifying it slightly, we get a new `<code>store.h</code>`:</p>
<pre>```
<code>extern int calls_made;

inline void log_call(void) {
  calls_made += 1;
}
</code>```</pre>
<p>If you think that `<code>inline</code>` implies no need for any sort of linking, you may be surprised:</p>
<pre>```
<code># gcc -std=c99 -c user.c -o user.o
# gcc -std=c99 -c clash.c -o clash.o
# gcc store.o user.o clash.o -o clash
/usr/bin/ld: user.o: in function `print_and_bump':
user.c:(.text+0xf): undefined reference to `log_call'
/usr/bin/ld: clash.o: in function `main':
clash.c:(.text+0xf): undefined reference to `log_call'
collect2: error: ld returned 1 exit status
# nm user.o | grep log_call
                 U log_call
</code>```</pre>
<p>I'm not going to dive into what exactly happened here, but it boils down to C99 designers wanting to avoid too many copies when inlining isn't possible. A symbol may still be needed for the function, as <a href="https://gustedt.wordpress.com/2010/11/29/myth-and-reality-about-inline-in-c99/">explained</a> by Jens Gustedt. That explanation offers a solution in the form of placing the following line into exactly one translation unit:</p>
<pre>```
<code>extern inline void log_call(void);
</code>```</pre>
<p>Alternatively, use the `<code>static</code>` version, although it may offer a different rate of success in inlining or emit too many copies.</p>
<h2>## Bad declaration</h2>
<p>Remember that symbols don't carry type information? One of the most confusing problems is using something believing it's one type when in reality it's another. For a quick example, see a botched version check in file `<code>version.c</code>`:</p>
<pre>```
<code>const char version[] = &quot;10.1&quot;;
</code>```</pre>
<p>The header `<code>version.h</code>` has a mistake! The variable is declared as `<code>int</code>` instead of `<code>char</code>`:</p>
<pre>```
<code>extern const int version;
</code>```</pre>
<p>The file `<code>check.c</code>` is using the declaration from the header:</p>
<pre>```
<code>#include &lt;stdio.h&gt;
#include &quot;version.h&quot;

int main(void) {
  printf(&quot;Version %d\n&quot;, version);
  return 0;
}
</code>```</pre>
<p>And when it tries to read the `<code>version</code>` variable…</p>
<pre>```
<code># gcc -c version.c -o version.o
# gcc -c check.c -o check.o
# gcc version.o check.o -o check
# ./check
Version 825110577
</code>```</pre>
<p>That doesn't look right… Nowhere during the entire process was the type of the actual variable compared to the one declared in the header. The compiler believed what was the header file, the linker matched the symbol simply by name, and the mistake only appeared when we tried to execute the program.</p>
<p>This time we got off easily, with an obvious problem. But the bugs can crash your code outright – if the type in the mistaken definition is larger than the actual type, or be subtle, if the wrong type is only slightly bigger and causes unrelated memory corruption, or when an `<code>int</code>` is taken for a `<code>float</code>`.</p>
<p>Your C++ compiler can catch some of those errors:</p>
<pre>```
<code># g++ -c check.c -o check.o
# g++ -c version.c -o version.o
# g++ version.o check.o -o check
/usr/bin/ld: check.o: in function `main':
check.c:(.text+0x6): undefined reference to `version'
collect2: error: ld returned 1 exit status
# nm version.o
0000000000000000 r _ZL7version
# nm check.o
0000000000000000 T main
                 U printf
                 U version
</code>```</pre>
<p>Unfortunately, C offers no solution but to always keep the headers correct!</p>
<h2>## Summary</h2>
<p>Those are the most common but annoying problems you may encounter which stem from the way C and symbols interact. If you're intrigued about some other linking problem, let me know about it. You can find me on the &quot;about me&quot; page.</p>
<h2>## Glossary</h2>
<p>Important terms this episode:</p>
<ul>
<li>*<em>inlining</em>* – copying a function's body instead of making a call</li>
<li>*<em>link-time optimization (LTO)</em>* – attempting to inline functions while linking, across translation unit boundaries</li>
</ul>

    
    <footer>
      <p>
        Written on <time datetime="2020-06-06">2020-06-06</time>.
      </p>
    </footer>
    <section>
        <a href="https://comments.dorotac.eu/posts/gcc_symbols">Comments</a>
        <iframe src="https://comments.dorotac.eu/posts/gcc_symbols" style="width: 100%;height: 100vh;" loading="lazy">
            <a href="https://comments.dorotac.eu/posts/gcc_symbols">Comments</a>
        </iframe>
    </section>
  </article>

    </main>
    <section id="global">
        <p><a href="/">dcz's projects</a></p>
        <p>Thoughts on software and society.</p>
        <div>
          <nav>
  <p>posts:</p>
  <ul>
    
        <li><a href="/posts/switch">Domain change</a></li>
    
        <li><a href="/posts/quick_maps">Maps à la carte</a></li>
    
        <li><a href="/posts/holes">Spots on the Sun and worn-out clothes</a></li>
    
        <li><a href="/posts/remote_debugging">Graphical debugging on the Librem 5 using QtCreator</a></li>
    
        <li><a href="/posts/jazda_rust">Jazda: Rust on my bike</a></li>
    
        <li><a href="/posts/why">Why Jazda?</a></li>
    
        <li><a href="/posts/git-botch-email">Git-botch-email</a></li>
    
        <li><a href="/posts/potion">A potion of experience</a></li>
    
        <li><a href="/posts/tabcheology">Browser tab archaeology</a></li>
    
        <li><a href="/posts/rust_maemo">Rust on Maemo</a></li>
    
        <li><a href="/posts/REing a pen">Reverse engineering a pen</a></li>
    
        <li><a href="/posts/in_the_middle">Start in the middle</a></li>
    
        <li><a href="/posts/nerd">When being a nerd paid off</a></li>
    
        <li><a href="/posts/flame_graph">Linux kernel flame graphs</a></li>
    
        <li><a href="/posts/fluwid">Simulating fluwids</a></li>
    
        <li><a href="/posts/blossom">A beautiful blossom of engineering excellence</a></li>
    
        <li><a href="/posts/objective">My objective function will blow it up</a></li>
    
        <li><a href="/posts/flat_earth">Geo in GL part 1: Flattening Earth</a></li>
    
        <li><a href="/posts/input_method">Wayland and input methods</a></li>
    
        <li><a href="/posts/embeddings">Word embedding fun</a></li>
    
        <li><a href="/posts/not_keyboard">It&#39;s not about keyboards</a></li>
    
        <li><a href="/posts/cargo_versions">Why I avoid Cargo: dependency versions</a></li>
    
        <li><a href="/posts/gcc_symbols">From C to exec: Part 3</a></li>
    
        <li><a href="/posts/dynamic_linking">From C to exec: Part 2</a></li>
    
        <li><a href="/posts/linking">From C to exec</a></li>
    
        <li><a href="/posts/brutalism">Brutalist blogging</a></li>
    
  </ul>
</nav>
          <p><a href="/atom.xml">Atom feed (no longer updated)</a></p>
        </div>
        <nav>
          <p>lists:</p>
          <ul>
            <li><a href="/">about me</a></li>
            <li><a href="/forges">my code</a></li>
          </ul>
        </nav>
    </section>
    <footer>
      <p>Articles published under the CC-BY-SA 4.0 license.</p>
    </footer>
  </body>
</html>