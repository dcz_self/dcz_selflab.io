---
title: "Git-botch-email"
date: "2022-03-13"
---

Git-botch-email
============

I don't really like git-send-email. I avoid projects that use it, if I can.

It could have been much better, but I (thankfully) use it rarely enough that I can't come up with its list of sins on the spot, and I don't end up getting my arguments acknowledged.

Today (2021-09-24) I have the dreadful need to send a patch to the Linux kernel, and I'm going to make the most of it by lining up my thoughts. Hopefully someone takes it to heart and creates a `git-sends-email-better`.

Email is all right
-------------------

Don't get me wrong, I like the decentralization aspect of email, I like that it's based on open standards, that I can use an arbitrary client to send it, and that I don't have to run random code on my computer just to submit or review patches. Forges fail at those, to various extents.

But git adds its own quirks
-----------------------------

I have a tree with my commit on top: it changed 3 lines. I want someone to include my changes on top of the official tree. Let's assume that I read the project's contribution guide, and I know that I need to use git-send-email, and I also know where to send the change. Let's begin.

```
$ git send-email 
No patch files specified!
git send-email [options] <file | directory | rev-list options >
[...]
```

"patch files"? I thought git operated on the basis of commits and trees. Why would I want to send a patch file using git? Anyway, there's a "rev-list" possiblity, so we can proceed with that:

```
$ git send-email 00082f898de21fd5ebb28dc561c173f6fde8e44a
/tmp/7yZAWWiApC/0001-media-imx-Fix-rounding.patch
To whom should the emails be sent (if anyone)?
```

I answer the questions, and then I get:

```
Send this email? ([y]es|[n]o|[e]dit|[q]uit|[a]ll):
```

Lol, no. It's my first submission, and I want to review it. Possibly by giving it to someone else first. I'm only human, and I make mistakes.

But where's the "save" option?

Okay, never mind. Maybe there is a "save" option further along the way. After all, I didn't give git any email access yet – I just want a dry run. It's not going to throw away the edits it offers you to perform, right?

```
Send this email? ([y]es|[n]o|[e]dit|[q]uit|[a]ll): y
sendmail: Cannot open mail:25
Died at /usr/libexec/git-core/git-send-email line 1497, <FIN> line 3.
```

…Actually, it totallydid throw them away. Thankfully I didn't make any edits (j/k, the first time in my life I tried to send a patch, I followed a tutorial and already wrote a heap of text at this step. That hurt).

Do you know how the cat keeps meowing but you have no idea what it wants? If only it could speak human language.

If only git-send-email could speak human language.

Some searching later, it turns out that, indeed, git wanted to have access to my SMTP server to send the email itself.

### Git sending emails

Let's get back to the save fiasco. I have a perfectly cromulent email client which can import .mbox or .eml files, and send them on. It's already configured for my email server, it can sign my emails, I trust it with my passwords, and it's customized to save sent emails to IMAP.

Why can't I save my git patch, and load it into my email client, to send it with my GPG signature?

Why should I trust git with full access to my email outbox? Why shouldn't I be able to use my usual Mail User Agent (MUA) to do the sending in a way that I pre-approved, respecting the security level I want to maintain, including TLS versions? Email is based on standards, after all.

I just hope Git doesn't handle the email password itself, given its [track](https://nvd.nist.gov/vuln/detail/CVE-2020-5260) [record](https://cve.mitre.org/cgi-bin/cvekey.cgi?keyword=git).

Oh, and don't forget that if you have a fancy multi-identity setup in your MUA, you have to duplicate it with git-send-email. Sure, git has a global config file. But If I configured that to my work account, then I'm one mistake away from sending a personal contribution to some other random repo from my work email. Double difficulty if you contribute under multiple different contexts.

So I'm stuck at having to [configure](https://www.freedesktop.org/wiki/Software/PulseAudio/HowToUseGitSendEmail/) every repo which demands git-send-email separately (use `--local` instead of `--global`. With git-send-email, opsec is harder.

Just give me the damn save already!

### Acceptance

Okay, I guess I have little choice in the matter. *Sigh*. Let's change the email to point to myself as the addressee. This will obviously not be a dry run for the final email because of the address mismatch, but hopefully nothing starts on fire when we change the addressee later.

![sent email](sent_email.png)

Great, it seems to look … odd. I was not expecting the patch to be part of the message. How am I supposed to download and apply it? By copying and pasting? It makes sense: those who reply can easily add their comments inline. I guess I can live with it. As long as I don't change the contents of the patch, it should be possible to extract.

### Intermission

But wait, git-send-email allows you to edit the message before sending, and mess it up in any way you want?!?

![screenshot of an email with an extra "From:" header in a terminal editor](botched_email.png)

```
Send this email? ([y]es|[n]o|[e]dit|[q]uit|[a]ll): y
OK. Log says:
[...]
Result: 250
```

LOL, git-send-email apparently doesn't do any validation. I hope that email didn't actually get forwarded anywhere. I put in "none" in the address field, but I dont really know well enough how git-send-email works. I've seen it add addresses. That wouldn't have worried me with my MUA.

### Back to the draft

Oh, right, maybe I can actually edit the draft from my MUA this time! If git-send-email doesn't try to stop me from being an idiot, I have no reason to use it now.

I copied the email from inbox to drafts, and added the timely message.

### Intermission 2

> Sometimes it's convenient to annotate patches with some notes that are not meant to be included in the commit message. [...] Such messages can be written below the three dashes "---" that are in every patch after the commit message.

So, if I want to include extra context inside my email, I should cram it in between lines of computer-readable text and hope for the best?

> ```
> The changes from 451a7b7815d0b pass 2^n to round_up, while n is needed.
>
> Fixes: 451a7b7815d0b
> ---
> Hi, I tested the patch on the Librem 5.
> drivers/staging/media/imx/imx-media-utils.c | 6 +++---
> 1 file changed, 3 insertions(+), 3 deletions(-)
>
> diff --git a/drivers/staging/media/imx/imx-media-utils.c b/drivers/staging/media/> imx/imx-media-utils.c
> index 5128915a5d6f..a2d8fab32a39 100644
> ```

That must be the best way ever invented to ensure people accidentally alter the wrong thing.

### Applying the patch

I was hoping not to have to go through the ordeal, but I don't want to bother my co-workers with my non-work blog. I'm being too nice.

I was joking when I said "copy-paste" the email content, and yet, this is how [Stack Overflow](https://stackoverflow.com/a/51995810) deals with it. And while I'm finding a bunch of guides for sending, that's the only answer dealing with receiving git-send-email results. Alas.

Surprisingly, it worked-for-me.

A couple exchanged emails later, and… I need to change the commit contents. Since the change was small, I just winged it, and altered the email directly, but if I actually had to go through the entire ordeal of git-send-email, I'd lose my changes again.

### Revisions

Finally, my email went to review. Of course, it didn't come out unscathed. I had to submit a second version, which consisted of multiple commits.

Here again, git-send-email showed how being careful results in paper cuts: each commit is sent as a separate email. That would have been okay of I could just load them into my draft folder and send from there, but, as we established earlier, the only way to do that is to send the email to myself. That means I have to change the recipient list for each email in the series manually. What botheration!

More tool problems
----------------------

Even after the patch was sent successfully, it's still less than a commit. It does not preserve the history of how the author got there. The base tree is discarded, and only the diff remains. Without an external convention, the reviewer does't even know which tree to apply the patch on, much less where it was originally tested. It's impossible to merge trees using git-send-patch either. There's not much "git" in it, really, because commits and trees are what git is made of.

Linux problems
------------------

There's a separate class of problems that are not the consequence of the tool, but the consequence of the culture which is often associated with the tool. It's fixable without any software changes, but it needs changes to wetware – which is probably even more difficult.

Here's a loose list, based on the Linux kernel:

How to find the correct address to direct the patches? How to find the correct tree to apply the patch on? Magical incantations are sometimes required like "Signed-off-by:" that have a meaning other than its constituent words (no, it doesn't mean your email is signed). Those things are documented, but they are not universal, and not obvious. And they are not difficult in the same way as writing a kernel patch is difficult – those are pure bureaucracy overhead, which does not have any bearing on code quality.

More overhead is in supporting ancient clients that can't handle MIME or compression. Those are banned in the kernel, instead of fixing clients to handle compression and inline disposition. Another exercise to support inflexible tools at the expense of human effort.

And the mother of all pet peeves: hard-wrapping of commit messages. Do you prefer to read prose by incessant scrolling, or with nonsense lines? Random example from the Linux kernel mailing list:

[Monospaced text hard-wrapped wider than the display, reader scrolls every line horizontally](scroll.webm)

![Prose with lines wrapped to random widths](wrapping.png)

This is what reading a patch would look like on the Librem 5 – depending on how you prefer to suffer – and it's also what any other pre-formatted text looks like. Including your commit messages. Have mercy and don't do hard wrapping.

It can be better!
-------------------

Those are, in the end, not unsurmountable problems, at least not all of them. *When used together with a competent email client*, git-send-email does its job passably. The only paper cut is not being able to export the emails without sending them.

It gets worse if there's no competent email client. Then the task of editing, reviewing, saving for later and coming back, and fixing mistakes falls entirely on the same tool. As far as I can tell, it's rather tedious, and the ability to get an overview is rather poor. There's no way to save and resume at all.

I'm not going to give it a score better than "passable" even with this papered over: it remains a fact that git-send-email almost encourages the submitter to accidentally mess up the patch by entering text in the wrong place. Until the payload is clearly delineated from the cover letter (as an inline attachment?), this cannot be solved.

* * *

**Some time later**
-------------------------

One of my patches was eventually accepted. The rest have been picked up by some poor soul who has been trying to get them upstreamed for the past 2 months.

git-send-email didn't get in the way any more, but I still messed up. I missed some feedback in my inbox, and thought the patches were completely forgotten, until the new person stepped in. You might blame my way of reading email, and you would be right: my email inbox is a mess. I've been trying to fix it for the past 5 years with little success. Meanwhile, I rarely lose feedback when it's placed on a web page, because it organizes conversations in a sensible manner.

Thankfully, that problem is not inherent to email, and has been solved by [sourcehut](https://sr.ht/). It closes the gap between an email-only workflow and a web-only forge by providing a single contact point, and displaying the knowledge (including historical data) in a decent manner. My analysis wouldn't be complete without mentioning it, and I ask projects using git-send-email to adopt it or something similar: I can't handle naked emails, and I'm not the only one!

But, being an internet service, sourcehut doesn't fix the problems in git-send-email. Perhaps they could come up with an improve version of that tool? I sure hope they could.