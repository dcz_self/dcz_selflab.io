<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Wayland and input methods</title>
    <meta http-equiv="Refresh" content="0; URL=https://dorotac.eu/posts/input_method" />
    <link rel="stylesheet" href="/style.css" />
  </head>
  <body>
    <section id="notice">
      This site moved to <a href="https://dorotac.eu/posts/input_method">dorotac.eu</a>. The version you're seeing now is a preserved copy from April 2023.
    </section>
    <main>

  <article>
    
      <h1># Wayland and input methods</h1>
<p>Wayland is gradually getting the ability to support input methods natively. Actually, it's plural &quot;abilities&quot;, because there are several pieces related to the functionality. Working in this area, I had to explain this to newcomers so often that I decided to write this blog post instead, to explain to everyone what's going on here, once and for all.</p>
<h2>## Text input</h2>
<p>Quick recap. The purpose of an input method is twofold: to give applications text from the user, and to recognize when and what kind of text is expected.</p>
<p><img src="purpose.svg" alt="Input methods help the user enter text in an efficient way" /></p>
<p>The most basic thing to do that under Wayland is the **<strong>text-input</strong>** protocol. It takes text from the compositor, and gives it to applications. It lets applications tell the compositor when and what kind of text they need. The protocol doesn't worry about the user, instead leaving that to the compositor.</p>
<p><img src="text_input.svg" alt="Text input connects applications to the compositor" /></p>
<h2>## Input method</h2>
<p>The compositor can take two paths in order to let user input the text. Either it takes the burden of communication on its own, by handling input itself, or it can delegate that task to some other program.</p>
<p><img src="input_method.svg" alt="Input method inserts itself between compositor and privileged program" /></p>
<p>In Wayland, the **<strong>input-method</strong>** protocol was designed to help. It is very similar to *<em>text-input</em>*, because it lets *<em>a program</em>* send text *<em>to the compositor</em>*, and allows *<em>the compositor</em>* to tell *<em>the program</em>* what kind of text is needed. Notice the inversion! This time, the program is special. It communicates with the user, and then gives the text to the compositor. The compositor is then typically going to send the text onward to the currently focused application using *<em>text-input</em>*, creating a chain: special program → compositor → focused application.</p>
<p><img src="focused.svg" alt="Input method is one, text inputs are many." /></p>
<p>This protocol has place for some additional responsibilities, too. Because there is typically only one application using this protocol, it can do things which would not work with multiple applications. One of them is grabbing the keyboard, known to CJKV language users. *<em>Input-method</em>* allows the special program to ask the compositor to send it all keyboard presses (&quot;exclusive grab&quot;). Taking over the keyboard makes it possible to send the text &quot;你好&quot; when &quot;nihao&quot; is typed. The other extension is meant for creating a special pop up window, which the compositor places next to the text field, and which can be used to show typing completions.</p>
<h2>## Virtual keyboard</h2>
<p>Input methods support would have been complete here, if all we cared about was text. However, the world is not so simple, and we have to deal with additional categories of input before being useful:</p>
<ul>
<li>text in legacy applications which don't support text-input, and</li>
<li>triggering actions which would normally need a keyboard.</li>
</ul>
<p>Both of them can be addressed by using a keyboard. But what if we're using a tablet computer, a TV, game console, or a phone, and there isn't one to speak of? We can address this issue by emulating button presses. Again, there are two basic ways to address this. The compositor can come up with something on its own, or it can delegate the task to another program.</p>
<p>The protocol **<strong>virtual-keyboard</strong>** is designed for programs which want to tell the compositor to issue &quot;fake&quot; keyboard button press events.</p>
<h2>## Together</h2>
<p>A fully-fledged input method program will be a Wayland client using the *<em>input-method</em>* protocol for submitting text, but also supporting *<em>virtual-keyboard</em>* for submitting actions, and as a fallback for legacy applications.</p>
<p><img src="virtual_keyboard.svg" alt="Virtual keyboard in parallel to input method" /></p>
<p>A compositor would ferry text around between the input method program and whichever application is focused. It would also carry synthetic keyboard events from the input method program to the focused application.</p>
<p>An application consuming text would support *<em>text-input</em>*, and it would send enable and disable events whenever a text input field comes into focus or becomes unfocused. It would also accept keyboard events.</p>
<p>Legacy applications won't send enable and disable, even when a text field is focused, and the user ready to type. When that happens, the compositor and the input method won't realize that text should be submitted now. If the input method uses an on-screen keyboard, it could remain hidden! Because of that, it's best to always make sure the user can bring up the input method and input text, which would then be delivered as keyboard events (which are always supported by applications).</p>
<h2>## Current state</h2>
<p>As of 2020-08-15, the latest versions of relevant protocols are:</p>
<h3>### <a href="https://gitlab.freedesktop.org/wayland/wayland-protocols/-/blob/master/unstable/text-input/text-input-unstable-v3.xml">*<em>text-input-unstable-v3</em>*</a></h3>
<p>Accepted in <a href="https://gitlab.freedesktop.org/wayland/wayland-protocols/">wayland-protocols</a>. Designed by me, based on <a href="https://gitlab.gnome.org/GNOME/mutter/-/blob/efd7a4af5e37299f17011a7f39cc66d8416a1bf9/src/wayland/protocol/gtk-text-input.xml">*<em>gtk-text-input</em>*</a> by Carlos Garnacho, and on <a href="https://gitlab.freedesktop.org/wayland/wayland-protocols/-/blob/master/unstable/text-input/text-input-unstable-v1.xml">*<em>text-input-unstable-v1</em>*</a>.</p>
<h3>### <a href="https://github.com/swaywm/wlroots/blob/master/protocol/input-method-unstable-v2.xml">*<em>input-method-unstable-v2</em>*</a></h3>
<p>Used in <a href="https://github.com/swaywm/wlroots/">wlroots</a>. Designed by me, based on <a href="https://gitlab.freedesktop.org/wayland/wayland-protocols/-/blob/master/unstable/text-input/text-input-unstable-v3.xml">*<em>text-input-unstable-v3</em>*</a> and <a href="https://gitlab.freedesktop.org/wayland/wayland-protocols/-/blob/master/unstable/input-method/input-method-unstable-v1.xml">*<em>input-method-unstable-v1</em>*</a>.</p>
<h3>### <a href="https://github.com/swaywm/wlroots/blob/master/protocol/virtual-keyboard-unstable-v1.xml">*<em>virtual-keyboard-unstable-v1</em>*</a></h3>
<p>Used in <a href="https://github.com/swaywm/wlroots/">wlroots</a>. Designed by me, based on the <a href="https://gitlab.freedesktop.org/wayland/wayland/-/blob/4e16ef0aed8db425afc8910b2a9708b57e165d39/protocol/wayland.xml#L2171">*<em>wl_keyboard</em>*</a> interface.</p>
<h2>## Future</h2>
<p>There are still some topics open. The most important one is about fixing the deficiencies in *<em>text-input</em>*, and updating *<em>input-method</em>* to match. Another one is regarding whether *<em>virtual-keyboard</em>* is even worth the effort, considering how it stirs up some conflict with Wayland's design.</p>
<p>Less important is implementing the additional features of *<em>input-method</em>*.</p>
<p>There's also the exploratory idea of designing a protocol dedicated to submitting actions like &quot;undo&quot;, &quot;submit&quot;, &quot;next field&quot;, but not text, in order to eliminate the need to emulate keys in modern keyboard.</p>

    
    <footer>
      <p>
        Written on <time datetime="2020-08-15">2020-08-15</time>.
      </p>
    </footer>
    <section>
        <a href="https://comments.dorotac.eu/posts/input_method">Comments</a>
        <iframe src="https://comments.dorotac.eu/posts/input_method" style="width: 100%;height: 100vh;" loading="lazy">
            <a href="https://comments.dorotac.eu/posts/input_method">Comments</a>
        </iframe>
    </section>
  </article>

    </main>
    <section id="global">
        <p><a href="/">dcz's projects</a></p>
        <p>Thoughts on software and society.</p>
        <div>
          <nav>
  <p>posts:</p>
  <ul>
    
        <li><a href="/posts/switch">Domain change</a></li>
    
        <li><a href="/posts/quick_maps">Maps à la carte</a></li>
    
        <li><a href="/posts/holes">Spots on the Sun and worn-out clothes</a></li>
    
        <li><a href="/posts/remote_debugging">Graphical debugging on the Librem 5 using QtCreator</a></li>
    
        <li><a href="/posts/jazda_rust">Jazda: Rust on my bike</a></li>
    
        <li><a href="/posts/why">Why Jazda?</a></li>
    
        <li><a href="/posts/git-botch-email">Git-botch-email</a></li>
    
        <li><a href="/posts/potion">A potion of experience</a></li>
    
        <li><a href="/posts/tabcheology">Browser tab archaeology</a></li>
    
        <li><a href="/posts/rust_maemo">Rust on Maemo</a></li>
    
        <li><a href="/posts/REing a pen">Reverse engineering a pen</a></li>
    
        <li><a href="/posts/in_the_middle">Start in the middle</a></li>
    
        <li><a href="/posts/nerd">When being a nerd paid off</a></li>
    
        <li><a href="/posts/flame_graph">Linux kernel flame graphs</a></li>
    
        <li><a href="/posts/fluwid">Simulating fluwids</a></li>
    
        <li><a href="/posts/blossom">A beautiful blossom of engineering excellence</a></li>
    
        <li><a href="/posts/objective">My objective function will blow it up</a></li>
    
        <li><a href="/posts/flat_earth">Geo in GL part 1: Flattening Earth</a></li>
    
        <li><a href="/posts/input_method">Wayland and input methods</a></li>
    
        <li><a href="/posts/embeddings">Word embedding fun</a></li>
    
        <li><a href="/posts/not_keyboard">It&#39;s not about keyboards</a></li>
    
        <li><a href="/posts/cargo_versions">Why I avoid Cargo: dependency versions</a></li>
    
        <li><a href="/posts/gcc_symbols">From C to exec: Part 3</a></li>
    
        <li><a href="/posts/dynamic_linking">From C to exec: Part 2</a></li>
    
        <li><a href="/posts/linking">From C to exec</a></li>
    
        <li><a href="/posts/brutalism">Brutalist blogging</a></li>
    
  </ul>
</nav>
          <p><a href="/atom.xml">Atom feed (no longer updated)</a></p>
        </div>
        <nav>
          <p>lists:</p>
          <ul>
            <li><a href="/">about me</a></li>
            <li><a href="/forges">my code</a></li>
          </ul>
        </nav>
    </section>
    <footer>
      <p>Articles published under the CC-BY-SA 4.0 license.</p>
    </footer>
  </body>
</html>