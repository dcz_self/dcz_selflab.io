---
title: "Start in the middle"
date: "2021-07-11"
---

Start in the middle
=============

It's a piece of advice I read years ago in the depths of the 'Net, which I can't find despite best efforts any more. It's surprising, because it's such an obvious guidance! Especially for those easily distracted, or prone to losing motivation.

TL;DR: When you work on something new, jump into the thick of it. Don't let yourself be sidetracked, and attack directly what interests you.

Storybuilding
----------------

Homer put Odysseus *in media res* as Odyssey begins: he's trying to return home after being shipwrecked. There's little backstory, or discussion about motivations. You focus on the current state, and wonder: how did he end up so? What challenges will he encounter? Can he succeed? You're hooked, and keep reading to see the questions answered.

It's good news for the distracted creator, because those are the same questions you may ask yourself when you have an awesome idea for a new project. What are the challenges? Can the idea succeed? You can get hooked on your own project by cutting out the fluff, and jumping straight into it.

Focus
-------

No one has built Skynet yet. If you are overly ambitious, chances are that you will have to build a lot of supporting infrastructure. Instead, choose the core element and stick to it. If you want to take pictures of all the graffitti in your town, don't bother with setting up a web site, just take photos. If your idea is to program a fancy reverb effect, don't bother turning it into a library.

In fact, don't bother with all the surroundings. Before you write all the READMEs, set up a documentation site, and publish the library on your favourite web site, you're risking that you run out of energy to actually do the thing you set out to do.

Side tracks
-------------

Sometimes you might want to build something that is so original that no one even built the tools you could use. A spaceship out of water bottles? Perhaps needs heavy duty glue, and all the glues you found are suboptimal. Suddenly, you're spending days researching chemistry, and the spaceship project is not moving anywhere.

Everything is cool if you enjoy your glue side project, but if your only motivation is to build a rocket, then perhaps you need to cut your losses and use one of the store glues. Get back to the middle: the spaceship, before you start hating the project! Once you nail the central part, either you're going to have so much motivation you'll deal with the side projects, or you will blissfully not care any more. Win-win!

Software
----------

The article is written about general projects, but software is especially prone to it, because there's so much complexity and potential challenges. I wouldn't have finished [fluw](/posts/fluwid) if I didn't take this approach. Thankfully, knowledge stays, and the more you build, the easier it will be to choose tools and minimize the time spent not in the middle of things.

Protagonist
-------------

You're the protagonist in your life, and challenge you create for yourself are stories you live. So take advice from the art of writing, start in the middle, and chances are that you're going to enjoy more what you do.