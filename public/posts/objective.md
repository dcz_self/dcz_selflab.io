---
title: "My objective function will blow it up"
date: "2020-11-29"
---

My objective function will blow it up
==========================

I have a preference for playing god. Build a system, observe, disrupt, see what new equilibrium it reaches. This manifests in weirdest places: gardening, cleaning, feeding the birds, computer games. Here's the story of how it turned me into a loss function and how you could become one too.

Computer games define the main thread today. It all played out in the span of mere weeks before publishing the post.

Fuel
-----

It started from an unexpected angle this time: not *Black and White*, where you literally play god to your people, nor *Civilization* where your actions determine the prosperity of a city. Instead, what set me on the path to explosion was *Crimsonland*, namely [the freeware 2002 version](http://phoenix.ee/old-crimsonland-1-02-1-3-0-1-4-0/). As I mourned that it's not playable under Linux, I considered making a Libre clone. However, after succumbing to a few more waves of monsters, I also concluded that it's perfect at what it does, therefore any potential attempt to replicate it would end up a great disappointment. No point trying. I let my attention wander.

![Crimsonland in action, credit Phoenix.ee](http://phoenix.ee/wp-content/uploads/2012/04/cl1-1024x788.png)

Oxidizer
-----------

*Creatures* is be a name you might have heard in the '90s and early '00s. A series of games that could be as sophisticated tamagotchi, they put you in charge of the well-being of humanoid creatures called Norns. I stumbled across a [gameplay video](https://www.youtube.com/watch?v=mxperoO4Kd8), which showed some gameplay, including sharing tips about punishing and rewarding Norns for their behaviour, breeding them, showing the contents of the neural network that makes their brains, and analyzing the chemicals flowing through their bodies. Turns out they have genes too!

![Norns splashing in the water. The pink one is about to jump off a pier, another is diving deep among the fishes. Credit: Wikipedia](Creatures_2_Norns.JPG)

To me, that moved the game from "being a parent" to "playing god" territory. I immediately tried it out. Turns out that playing a nurturing god is only exciting when there's something going on, and the Norns learn and explore rather slowly. As I let them be, as they walked back and forth on a second screen, my attention went astray again…

Detonator
------------

My next chance encounter was the [link](https://news.ycombinator.com/item?id=24983956) to Bevy, the game engine written in Rust. Nice, I've been wanting to learn to use an Entity Component System for a while, and I'm unwilling to deal with C. I never made games because of how tedious basic graphics are, but if I had anything to use it for, I would give it a try.

Fire in the hole!
-------------------

And then it all clicked.

What if I took Crimsonland and gave the monsters a set of genes – simple enough to let them sense the enemy and walk – and culled them using the protagonist's weapons, it would be artificial selection!

Selection by gun
--------------------

What's so cool about that? It's that the programmer is no longer directly in control. A well executed evolutionary algorithm may take you to unexpected places. It may be undesired when it subverts your goals and [do something you didn't want it to](https://deepmind.com/blog/article/Specification-gaming-the-flip-side-of-AI-ingenuity) – there was a group who tried to teach a simulated human to throw a ball, but ended up with one *running with it* instead (sadly, I lost the link). It may be neutral when the shape of its solution is [just weird](https://en.wikipedia.org/wiki/Evolved_antenna), or, like I hope to see here, it may bring surprise and variety into the game. That's on top of the fact that *I don't really know* how to program a smart AI for a monster myself.

![Evolved antenna looking like a randomly bent wire](https://upload.wikimedia.org/wikipedia/commons/thumb/f/ff/St_5-xband-antenna.jpg/220px-St_5-xband-antenna.jpg)

Now that we know what evolution is good for, what are the pieces it requires to take off?

First, we need a bunch of creatures: the gene pool. That's a given both in the virtual world and on farms. Evolution does not work on individuals, but on groups, and it's the group of individuals that keeps improving.

Next, in order for the group to improve, there needs to be a way to know the "better" ones from "worse" ones. For a farmer, this may be the number of laid eggs. For a shooter, let's start with the rule that every creature (genome) that manages to score a bite gets a "goodness" point. That's also called the "objective function".

It's no use knowing what is better if we don't do something about. We want improvement in our gene pool, so genomes scoring higher will more often be the source of newly spawned creatures, and those less bitey will be underrepresented. That keeps the balance on the high side.

But those spawned creatures will not just be genetic copies of their ancestors. If they were, our gene pool would only be able to lose genetic variety: some heritage lines (the unsuccessful ones) would die off forever, leaving emptiness in its place. So we need to restore some of that variety. Instead of spawning exact clones, we give them a chance to mutate! Less bitey mutations rarely score high, and so they rarely reproduce. Little harm done. However, better monsters score high often, and they will spawn copies more often – gene pool improvement!

We end up with a dominating loop: creature -> bite -> mutate -> better creature -> bite -> mutate. There are other paths which arrive at a dead end, like: creature -> bite -> mutate -> worse creature -> fail, or bad creature -> fail -> not breed. As long as the loop dominates, however, we're improving.

There, now we are on the path to breed ultimate killing machines, and I can play god with an entire species of virtual beings.

Breedmatic
--------------

The result of that all of thinking is a prototype game [Breedmatic](https://github.com/dcz-self/breedmatic). Adhering to my principle of starting in the middle, from the most interesting part, I went on to create a bunch of genetically modifiable monsters. They are very simple: sense, turn, walk. The genes describe how they turn relative to their target: their "brain". There's one hidden part of the story though: breeding and maintaining a healthy population.

It turns out that implementing a good genetic algorithm takes a lot of time. There are considerations regarding the size of gene pools, the rates of mutation of different parts of the genome, the selection of the best objective function, and even the strategy for preserving diversity: should reproduction be asexual? Diploid? How many sexes? That is a lot of sessions of shooting monsters. Hands get tired, progress is slow. In a moment of inspiration, I decided to sidestep the problem with… an evolutionary algorithm. As of version 0.2, the shooter itself is driven by evolution, in much similar way as the monsters.

But they start blank, so you can watch the shooters over hundreds of attempts going from barely moving through phases of spinning randomly to somewhat competent, and even sometimes coming up with weird moves which end up scattering the otherwise accurate laser, which end up being nonsensically effective against the incoming hordes.

[A video of an evolved shooter that actually kinda gets it. Seizures help when shooting lasers, apparently.](https://porcupinefactory.org/data/breed3.mkv)

Playing god has never been so comfy: no need to even move a finger.

Is this fun tho?
------------------

Sure it is. Watching the struggle of each and every shooter, I can't help but pray to the random number generator gods for luck in the next draw. "May it be 55's offspring next! It was turning the right way, just opposite!". The chance of hitting gold with the brain connections keeps me on the edge of the seat, and I lose track of time. Every session, there's a chance that a previously unseen combination appears – and the brain is only made of 7 neurons!

But you promised I could be the objective function!
-------------------------------------------------------------

And so you can! The most recent version from git lets you take over control. The monsters are not bred for variety, and the gameplay is basic though, so it's rather boring. At least compared to watching evolution go, so I have a better idea going in that direction.

Did you play [Massive Chalice](http://www.massivechalice.com/)? It combines a tactical game with the management of a royal lineage. There are not many games of this sort, and the premise is effectively the same as mine here: to end up with a genetically superior population. Following the inspiration, future versions of Breedmatic will aim to put the player – not the algorithm – in the role of the gene pool manager. Hopefully that's more fun than shooting questionably intelligent monsters.

Evolve
--------

Breedmatic is open, so if you are dismayed with the intelligence or shape of the creatures, feel free to fork it or contribute. Perhaps we can share ideas regarding the actual gameplay. The prototype could still evolve in many directions!

Otherwise, watch this space. While the next post may not be about evolution, I always have some crazy project in progress.