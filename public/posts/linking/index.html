<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>From C to exec</title>
    <meta http-equiv="Refresh" content="0; URL=https://dorotac.eu/posts/linking" />
    <link rel="stylesheet" href="/style.css" />
  </head>
  <body>
    <section id="notice">
      This site moved to <a href="https://dorotac.eu/posts/linking">dorotac.eu</a>. The version you're seeing now is a preserved copy from April 2023.
    </section>
    <main>

  <article>
    
      <h1># From C to exec</h1>
<p>As the idea of having a technical blog grew in my mind and came closer to reality, it became obvious that it needs to host some kind of useful content apart from fluff and opinions. For this, I asked a few of my many technical friends:</p>
<blockquote>
<p>&gt; What can I write about for you?</p>
</blockquote>
<p>The suggestion that I liked the most was to write about the process that turns a C source file into an executable. The topic seems quite opaque, and indeed I don't recall seeing many ways to get into it without already having some background knowing about linking, translation units, and symbols.</p>
<p>The goal of this post is to establish this background for a reader who has a passing familiarity with C.</p>
<p>The seasoned reader should be warned that this is not a rigorous summary of the C standard, but rather a gentle introduction to the practical world of linking ELF files on GNU/Linux with GCC. Simplicity prevails, so while the C standard gives some room for different results, we'll explore only the case of unoptimized code and GCC version 9.</p>
<h2>## Act 1: Hello World</h2>
<p>If you've dealt with C before, you're probably familiar with the `<code>hello.c</code>` file.</p>
<pre>```
<code>#include &lt;stdio.h&gt;
int main(void) {
  puts(&quot;Hello World!&quot;);
  return 0;
}
</code>```</pre>
<p>The process of turning code into an executable usually consists of at least 2 main steps: *<em>compilation</em>* and *<em>static linking</em>*. Depending on how you've usually done your programming, you may be familiar with different ways of performing those two steps. Let's take a look at two most popular ones.</p>
<h3>### Scene 1: Executable</h3>
<p>This unsuspecting source file is about to undergo transformations that push it through the entire pipeline, changing it beyond recognition into a dynamic executable. Prepare your terminals:</p>
<pre>```
<code># gcc hello.c -o hello
# ./hello
Hello World!
# file hello
hello.o: ELF 64-bit LSB executable, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, for GNU/Linux 3.2.0, BuildID[sha1]=c1b39511769ab152611cd8ad83983dc724333b7a, not stripped
</code>```</pre>
<p>Congratulations, we've turned code into an excutable!</p>
<h3>### Scene 2: Linking</h3>
<p>But wait, wasn't this supposed to be 2 steps? Indeed, gcc does them both at the same time when not passed the `<code>-c</code>` flag. Let's try again:</p>
<pre>```
<code># gcc -c hello.c -o hello.o
# ./hello.o
bash: ./hello.o: Permission denied
# file hello.o
hello.o: ELF 64-bit LSB relocatable, x86-64, version 1 (SYSV), not stripped
</code>```</pre>
<p>That's not the same kind of file as before! What we got is an object file instead: a compiled version of the source code, without any linking applied. Notice the &quot;relocatable&quot; instead of &quot;executable&quot;.</p>
<p>Let's link it now:</p>
<pre>```
<code># gcc hello.o -o hello
# ./hello
Hello World!
</code>```</pre>
<p>That's an executable again!</p>
<p>You may wonder: what happened? how is the intermediate file different from the executable? why is it useful? Let's move on to the second act...</p>
<h2>## Act 2: Symbols</h2>
<p>Here we discover why linking is useful and necessary.</p>
<p>Our project is growing more complicated. It's composed of two source files now.</p>
<p>`<code>hello.c</code>` morphs:</p>
<pre>```
<code>#include &quot;print.h&quot;
int main(void) {
  print_hello();
  return 0;
}
</code>```</pre>
<p>`<code>printer.c</code>` contains:</p>
<pre>```
<code>#include &lt;stdio.h&gt;
void print_hello(void) {
  puts(&quot;Hello World!&quot;);
}
</code>```</pre>
<p>Finally, they are joined by `<code>print.h</code>`:</p>
<pre>```
<code>void print_hello(void);
</code>```</pre>
<h3>### Scene 1: Object files</h3>
<p>This is a more complicated project. A function from one file is referenced in another! And there's a new header file as well. How do we turn this into an executable? Of course, we could just use `<code>gcc</code>` without the `<code>-c</code>` flag, and hope that it figures out everything on its own. But that's not why we're here, so let's go step by step:</p>
<p>This compiles the `<code>printer.c</code>` file into an object file `<code>printer.o</code>`:</p>
<pre>```
<code>gcc -c printer.c -o printer.o
</code>```</pre>
<p>So far so good. This produces the `<code>hello.o</code>` file, which is compiled `<code>hello.c</code>`:</p>
<pre>```
<code>gcc -c hello.c -o hello.o
</code>```</pre>
<p>Wait a minute… `<code>hello.c</code>` calls a function in `<code>printer.c</code>`, but neither the command, nor `<code>hello.c</code>` reference `<code>printer.c</code>` anywhere! All they have is the name `<code>print_hello</code>`, but not the body of the function. How does the compiler know what call to insert?</p>
<p>Turns out that the compiler doesn't actually need to resolve all names to function bodies immediately. What it does instead is use *<em>symbols</em>* to step around the problem. When a source file containing a function (or a global variable) named `<code>print_hello</code>` gets compiled, the resulting object file (or a library) gets a symbol called `<code>print_hello</code>`. On the other hand, any object file *<em>using</em>* that symbol creates a reference to it (in the *<em>relocation table</em>*), to be filled later: at linking time.</p>
<p>Let's see it for ourselves:</p>
<pre>```
<code># nm printer.o
0000000000000000 T print_hello
                 U puts
</code>```</pre>
<p>According to `<code>man nm</code>`, `<code>T</code>` means the symbol is defined and in the *<em>Text</em>* section. What about the other object file?</p>
<pre>```
<code># nm hello.o
0000000000000000 T main
                 U print_hello
</code>```</pre>
<p>As expected, `<code>print_hello</code>` is referenced, but `<code>U</code>` for &quot;Undefined&quot;. It's simply not in this file.</p>
<p>To fill in the vacancy, let's do the final step, and link both files together:</p>
<pre>```
<code># gcc hello.o print.o -o hello
# nm hello
[...]
0000000000401126 T main
0000000000401136 T print_hello
[...]
</code>```</pre>
<p>There are many more symbols, but the one we're interested in is there: `<code>print_hello</code>` is now present. Notice that `<code>main</code>` is present as well, meaning that we have the bodies of *<em>both</em>* of our source files in the same compiled file now.</p>
<p>The linker has performed *<em>relocation</em>*. If you remember from before, the *<em>object file</em>* type returned by the *<em>file</em>* command was `<code>relocatable</code>`. Our binary is now *<em>executable</em>*, meaning that we can't repeat the same operation again to add some symbols we forgot about any more. But the linker will not let you forget anything anyway:</p>
<pre>```
<code># gcc hello.o -o hello
/usr/bin/ld: hello.o: in function `main':
hello.c:(.text+0x5): undefined reference to `print_hello'
collect2: error: ld returned 1 exit status
</code>```</pre>
<h3>### Scene 2: Header files</h3>
<p>The other anomaly introduced in this example is the header file. It contains only one line:</p>
<pre>```
<code>void print_hello(void);
</code>```</pre>
<p>What is it for? Suppose we didn't have it. The only information about `<code>print_hello</code>` would come from its call, which looks like this:</p>
<pre>```
<code>  print_hello();
</code>```</pre>
<p>Can you guess the type of the function? Well, sort of: it takes no arguments, but it may return anything. And if we made a mistake, like this:</p>
<pre>```
<code>  print_hello(&quot;world&quot;);
</code>```</pre>
<p>a compiler guessing the type of arguments would happily go on trusting what we wrote, instead of warning us of the extra argument. The line in the header file, called *<em>prototype</em>*, informs the compiler about which functions are available, and what type they have.</p>
<p>As an aside, you might notice that this was already partially covered by the linker: when function `<code>print_hello</code>` was unavailable in our example, we received am &quot;undefined reference&quot; message from the linker. Couldn't we get rid of headers and let the linker catch those errors? Indeed, gcc-c++ uses some additional information to store function type:</p>
<pre>```
<code># cp printer.c printer.cpp
# g++ -c printer.cpp -o printerpp.o
# nm printerpp.o 
                 U puts
0000000000000000 T _Z11print_hellov
# c++filt _Z11print_hellov
print_hello()
</code>```</pre>
<p>This is called *<em>symbol mangling</em>*, and C compilers don't generally use it, leaving us with obligatory prototypes, and, in practice, header files.</p>
<p>Despite this, C++ still requires prototypes inside the header files, for reasons that are not clear to me. I suspect compatibility with C, or even IDE suggestions.</p>
<h3>### Scene 3: Static libraries</h3>
<p>Static libraries are used when additional functionality provided by third party is needed. They also provide symbols, and may depend on other libraries. If this sounds familiar, it's because it is! That's the same core functionality as object files.</p>
<p>In fact, if you look at a shared library on Linux (file name ending with `<code>.a</code>`), it's just an *<em>ar</em>* archive containing multiple object files.</p>
<pre>```
<code># ar -t /usr/lib64/libm-2.29.a
s_lib_version.o
s_matherr.o
s_signgam.o
fclrexcpt.o
fgetexcptflg.o
fraiseexcpt.o
[...]
</code>```</pre>
<p>Let's turn our printer into a static library and see for ourselves:</p>
<pre>```
<code># gcc -c -o printer.o printer.c
# ar rcs libprinter.a printer.o
# nm libprinter.a

printer.o:
0000000000000000 T print_hello
                 U puts
# gcc hello.o libprinter.a -o hello
# ./hello
Hello World!
</code>```</pre>
<h2>## Intermission</h2>
<p>Those steps are sufficient if you need to create a binary for the bare metal, for example for an Arduino with an AVR microcontroller, or if you're creating a basic operating system yourself. The executable needs to be provided in the final form, taking minimal advantage of libraries present on the system. Anything with an operating system permitting *<em>dynamic (shared) libraries</em>* (like Windows or Linux) is likely to have a 3rd step, which happens immediately before execution: *<em>dynamic linking</em>*. We will cover that in a future chapter, together with various gotchas related to how C emits symbols.</p>
<h2>## Glossary</h2>
<p>*<em>compilation</em>*: turning a *<em>source</em>* representation of a program into *<em>machine code</em>*</p>
<ul>
<li>*<em>executable</em>*: a runnable program</li>
<li>*<em>header file</em>*: a file in the C language containing, among others, *<em>prototypes</em>*</li>
<li>*<em>linking</em>*: combining different *<em>object files</em>* together</li>
<li>*<em>object file</em>*: a *<em>compiled</em>* form of the *<em>source</em>*</li>
<li>*<em>prototype</em>*: a piece of text telling the compiler about the existence of a certain *<em>symbol</em>* and its type</li>
<li>*<em>relocation</em>*: filling *<em>symbols</em>* from one *<em>object file</em>* into others</li>
<li>*<em>relocation table</em>*: a list of references to missing symbols</li>
<li>*<em>source code</em>*: text written in a human-readable form, e.g. in the C language</li>
<li>*<em>static library</em>*: a collection of *<em>object files</em>*</li>
<li>*<em>symbol</em>*: a piece of program that can be accessed from another *<em>object file</em>*</li>
</ul>

    
    <footer>
      <p>
        Written on <time datetime="2020-05-28">2020-05-28</time>.
      </p>
    </footer>
    <section>
        <a href="https://comments.dorotac.eu/posts/linking">Comments</a>
        <iframe src="https://comments.dorotac.eu/posts/linking" style="width: 100%;height: 100vh;" loading="lazy">
            <a href="https://comments.dorotac.eu/posts/linking">Comments</a>
        </iframe>
    </section>
  </article>

    </main>
    <section id="global">
        <p><a href="/">dcz's projects</a></p>
        <p>Thoughts on software and society.</p>
        <div>
          <nav>
  <p>posts:</p>
  <ul>
    
        <li><a href="/posts/switch">Domain change</a></li>
    
        <li><a href="/posts/quick_maps">Maps à la carte</a></li>
    
        <li><a href="/posts/holes">Spots on the Sun and worn-out clothes</a></li>
    
        <li><a href="/posts/remote_debugging">Graphical debugging on the Librem 5 using QtCreator</a></li>
    
        <li><a href="/posts/jazda_rust">Jazda: Rust on my bike</a></li>
    
        <li><a href="/posts/why">Why Jazda?</a></li>
    
        <li><a href="/posts/git-botch-email">Git-botch-email</a></li>
    
        <li><a href="/posts/potion">A potion of experience</a></li>
    
        <li><a href="/posts/tabcheology">Browser tab archaeology</a></li>
    
        <li><a href="/posts/rust_maemo">Rust on Maemo</a></li>
    
        <li><a href="/posts/REing a pen">Reverse engineering a pen</a></li>
    
        <li><a href="/posts/in_the_middle">Start in the middle</a></li>
    
        <li><a href="/posts/nerd">When being a nerd paid off</a></li>
    
        <li><a href="/posts/flame_graph">Linux kernel flame graphs</a></li>
    
        <li><a href="/posts/fluwid">Simulating fluwids</a></li>
    
        <li><a href="/posts/blossom">A beautiful blossom of engineering excellence</a></li>
    
        <li><a href="/posts/objective">My objective function will blow it up</a></li>
    
        <li><a href="/posts/flat_earth">Geo in GL part 1: Flattening Earth</a></li>
    
        <li><a href="/posts/input_method">Wayland and input methods</a></li>
    
        <li><a href="/posts/embeddings">Word embedding fun</a></li>
    
        <li><a href="/posts/not_keyboard">It&#39;s not about keyboards</a></li>
    
        <li><a href="/posts/cargo_versions">Why I avoid Cargo: dependency versions</a></li>
    
        <li><a href="/posts/gcc_symbols">From C to exec: Part 3</a></li>
    
        <li><a href="/posts/dynamic_linking">From C to exec: Part 2</a></li>
    
        <li><a href="/posts/linking">From C to exec</a></li>
    
        <li><a href="/posts/brutalism">Brutalist blogging</a></li>
    
  </ul>
</nav>
          <p><a href="/atom.xml">Atom feed (no longer updated)</a></p>
        </div>
        <nav>
          <p>lists:</p>
          <ul>
            <li><a href="/">about me</a></li>
            <li><a href="/forges">my code</a></li>
          </ul>
        </nav>
    </section>
    <footer>
      <p>Articles published under the CC-BY-SA 4.0 license.</p>
    </footer>
  </body>
</html>