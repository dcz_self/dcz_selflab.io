<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Jazda: Rust on my bike</title>
    <meta http-equiv="Refresh" content="0; URL=https://dorotac.eu/posts/jazda_rust" />
    <link rel="stylesheet" href="/style.css" />
  </head>
  <body>
    <section id="notice">
      This site moved to <a href="https://dorotac.eu/posts/jazda_rust">dorotac.eu</a>. The version you're seeing now is a preserved copy from April 2023.
    </section>
    <main>

  <article>
    
      <h1># Jazda: Rust on my bike</h1>
<p>*<em>This blog post is an improved version of an impromptu talk I gave at FrOSCon two weeks ago.</em>*</p>
<p>I like to do things that don't quite make sense. One of them is putting rust on my bicycle.</p>
<p><img src="bike.png" alt="The front of a dirty purple bicycle. In the background, windows of a building which carries the FrOSCon logo." /></p>
<p>This is my bicycle, and it's made out of steel. It has some rust already, but it's not the kind of &quot;rust&quot; I want to talk about.</p>
<p><img src="tube.png" alt="The seat tube shown from above. On the outside, it's covered with light dirt, and on the inside, the colour is dirty orange" /></p>
<p>I put Rust the programming language on my bike.</p>
<h2>## Bicycle computer</h2>
<p>I bought my first bicycle computer before I could even program. It was a simple mechanical device that is mounted near the wheel. It counts wheel rotations, and displays a distance.</p>
<p><img src="mechanical.png" alt="A metal box with a counter behind a window. On the left, there's a propeller-like element. On the bottom there's a mounting bracket. The display is made of 4 black 0s and one red ring to the right. The red digit reads 3, and partially rotated away, so that the bottom is not in the window's frame." /></p>
<p>My next upgrade was an electronic device, one unlike what you could buy today. Those have more functionality: they may display distance, speed, and time travelled. I used one for over a decade, and it was possibly the best spent 20 EUR in my life.</p>
<p><img src="electronic.png" alt="A squareish plastic display unit. It has a wide button on the top edge and another one on the bottom edge. In between, taking most of the space, is a transparent window with a LCD display. 7-segment digits show &quot;0&quot;: one on the top labelled &quot;KMH, and 3 on the bottom reading &quot;0.00 TRP&quot;" /></p>
<p>Two things happened between then and now. One is that I learned to code. The other is that the battery ran out. Instead of replacing the battery, I came to the obvious conclusion: I can code, so I can make a better one!</p>
<h2>## Hardware</h2>
<p>I've been struggling to design the hardware for my bike computer for years. Sadly, I suck at hardware design. I can't solder, I can't design a plastic case, I can't build a device that won't shake apart during a most peaceful ride. It became clear that if I want a bike computer, I should start with existing hardware.</p>
<p>Meanwhile, the situation in the outside world slowly changed. Smartphones gained popularity, Internet of Things followed, smart watches started utilizing the new tiny and powerful components. Bicycle computers started taking advantage of the new powers too.</p>
<p>I'm much better at programming than at hardware, so I stopped to consider: should I take advantage of that influx of hardware, and adapt an existing device to my needs?</p>
<p>I could slap a smartphone on the handlebars, write an application and call it a day. Or I could find a smaller device, more like the bicycle computer I retired, and use it as the base for my software. But which path should I choose?</p>
<p><img src="modern.png" alt="A rounded-rectangular gadget, showing a table of values on a black-and-white LCD: speed, stopwatch, distance, time, heart rate, cadence. It has 4 small buttons, 2 on the left and 2 on the right." /></p>
<p>There are many cyclists in the world. There are those who commute to work every day, others who go on family trips, some cycle to deliver things, and yet some like to race. They have different goals and needs while cycling, and each is interested in something different from a cycling computer.</p>
<p>This time, I'm building one that makes *<em>me</em>* happy. Which of my needs as a cyclist can a bike computer help with?</p>
<p>I usually cycle in places I roughly know, so I don't need a map, or a big, fragile screen to display one. I want to time my rides, so I want a screen that's big enough to show a few numbers in a big font, and the bike computer must be sturdy enough when I push the tempo over rocky trails. When I cycle, I leave my phone at home, so I won't miss anything if the bike computer is 100% offline. I draw on maps for OpenStreetMap, so I want some sensors like GPS, and a place to store their data. Thankfully, handling sensors and statistics doesn't require much computational power.</p>
<p>A smartphone meets those needs, but has some important downsides. Most smartphones aren't well readable in sunlight. They have short battery life – days compared to weeks – and they are not very resilient compared to other gadgets. I often get caught in the rain, and I'm pretty sure too much road dust or violent shaking is not healthy for smartphones – even if they don't come flying out of the harness.</p>
<p>The conclusion is pretty clear: I would be better served by a dedicated device. And so I started my search.</p>
<p>However, I'm only a single hacker, with limited time and abilities. I want to build a bike computer from scratch, I have another need – as a hacker, not cyclist: the device must be simple enough so that I can hack on it myself.</p>
<p>And I did see some interesting examples, like <a href="https://www.reddit.com/r/linuxmasterrace/comments/ehn19r/how_to_hack_stages_cycling_dash_l50_or_m50/">one based on Linux</a>. I rejected this one purely because I was afraid about the hardware complexity – if the manufacturer decided they needed a full-blown OS, then there must be a lot of hardware to manage there.</p>
<p>Finally, I found one. The <a href="https://www.espruino.com/Bangle.js2">Bangle.jS 2</a> smart watch checked most of the boxes: lots of sensors, Bluetooth Low Energy, a screen readable in the sunlight, and even a GPS receiver! It only has one button, and it can't make sounds, but it was reverse engineered, and ready to flash with custom software. It was then or never: if I didn't use this as my base, I would probably never finish the bike computer project.</p>
<h2>## Rust</h2>
<p>There are lots of choices in the embedded space. Operating systems like <a href="https://nuttx.apache.org/">Nuttx</a>, <a href="https://www.espruino.com/">Espruino</a>, <a href="https://os.mbed.com/mbed-os/">Mbed OS</a>, <a href="https://www.riot-os.org/">Riot</a>, <a href="https://www.zephyrproject.org/">Zephyr</a>. Or even running on bare metal. But my goal was clear:</p>
<p><img src="rust-logo-blk.svg" alt="Rust-lang logo" /></p>
<p>I wrote enough C code to know I don't want to use it. The 2 Rust-enabled options were Riot and bare metal using <a href="https://github.com/rust-embedded">rust-embedded</a> crates.</p>
<p>Actually, there were 3 options. At the last moment, I discovered <a href="https://tockos.org/">Tock</a>, an OS written entirely in Rust. It has an advantage over all other options (except Espruino): it's a pre-emptive operating system, able to load applications at runtime.</p>
<p>And loading new apps is a standard function of your computer, of your smart phone, and some smart watches. Why isn't this standard on bike computers yet? Puzzling, but if the sports gadgets manufacturers won't do that, I gladly will.</p>
<h2>## Tock project</h2>
<p><img src="apps.svg" alt="Diagram showing 2 layers: Tock kernel and multiple apps on top: speed meter written in Rust, clock written in C, and space for more." /></p>
<p>Applications made for Tock are native code, can be written in C or Rust. Because of the multiprocessing architecture of Tock, we can split functionality into apps, like a speed meter, or a clock, and not worry how buggy they are: they may crash all they want, but they are separated from each other, so a crashing clock won't bring your down speed display.</p>
<p>Imagine a future where people load applications from the internet on the bike computer. Being able to just ignore a crashing app will be absolutely necessary.</p>
<p>But this kind of safety comes with a cost. You have to write a lot more code to abstract hardware resources. Instead of writing just one device driver, you actually need to write 3 pieces: one kernel driver, one userspace driver, and one multiplexer.</p>
<p><img src="stack.svg" alt="Diagram showing the 3 layers: hardware, kernel, app. Hardware is the GMC303 chip. On top in the kernel, GMC303 driver underlies the compass syscall driver. On top o that, as part of the map app, there's the compass API" /></p>
<h2>## Status</h2>
<p>Despite the slower pace, I managed to put Tock OS on the Bangle.js 2 hardware rather quickly. I started with a demo displaying speed:</p>
<p><img src="speed.png" alt="A smart watch on a wooden background. It reads &quot;14&quot;, and there's a 30° arc to the left of the number, centered on the number. There's a small &quot;33&quot; in the lower right corner." /></p>
<p>The demo is part of the <a href="http://jazda.org">Jazda project</a> (which is what I called the bike computer), and it shows that the display stack is working, and that the GPS stack is working.</p>
<p>There's still lots of work before the grand vision can be realized. The main parts are:</p>
<ul>
<li>Bluetooth support</li>
<li>Concurrency: there are compiler shortcomings that prevent this part of the OS from really gaining speed</li>
<li>Communication between apps</li>
<li>Communication with a computer</li>
<li>Publishing apps online: a website, payment service</li>
</ul>
<p>Jazda started as a hobby project, so I never forget about fun projects:</p>
<ul>
<li><a href="https://gitlab.com/dcz_self/seismos">Seismos</a>, the sensor collection file system</li>
<li><a href="https://framagit.org/jazda/core/-/tree/master/ray-graphics">Ray-graphics</a>, the <a href="https://www.ronja-tutorials.com/post/034-2d-sdf-basics/">SDF</a>-based graphics library</li>
<li>Skitram, the unpublished time-series data compression library</li>
</ul>
<h2>## GPS vs BLE</h2>
<p>Currently, the greatest shortcoming of Jazda is the lack of Bluetooth Low Energy. This is a wireless protocol normally used by bicycle sensors. What Jazda is using right now is GPS readings. Unfortunately, GPS is rather energy-hungry, which means the device can only display speed for 5 hours before turning off, and the readouts aren't very accurate, either.</p>
<p>Tock has a strict policy of not allowing direct hardware access, so I can't just snatch an external BLE stack. If I did that, I would be stuck maintaining the result myself, because it wouldn't be accepted upstream. Alternatively, I could write a BLE stack myself, but Bluetooth is notoriously difficult to implement correctly.</p>
<p>For now, I'll just keep working on the other parts.</p>
<h2>## Innovation</h2>
<p>Thankfully, speed readouts are overrated. There are plenty of other things that can be done without them. I collected some ideas:</p>
<ul>
<li>drawing a situational map</li>
<li>wheelie detection</li>
<li>surface roughness scanning for OpenStreetMap</li>
<li>sonar mode for chasing a recorded ride</li>
<li>???</li>
<li>profit</li>
</ul>
<p>Some of them come from myself, some were suggestions I heard. Perhaps I won't be able to implement them all, but that's fine, because Jazda is open source software. Anyone is allowed to implement any crazy idea without signing an NDA and without the need to find a job at a sports equipment company. Having a shower thought and some perseverance is all that's necessary.</p>
<h2>## Community</h2>
<p>But it's always easier to hack when other people can help, so you're invited to join our chat on Matrix or IRC: #jazda:<a href="https://web.libera.chat/">libera.chat</a> . You can also reach out to me there to order development kits for Jazda. Those come with a unique breakout board to make it easy to reflash the Bangle.js 2 smart watch with the Jazda firmware.</p>
<p><img src="devkit.png" alt="A USB programmer connected via a ribbon cable to a breakout board, connected to a USB cable that ends outside of the picture." /></p>
<p>If you're a hardware hacker, you're especially welcome. Perhaps you could help us build a future version of Jazda, without *<em>any</em>* shortcomings… and without useless heart sensors ;)</p>
<p><a href="https://jazda.org"><img src="jazda.svg" alt="Jazda logo: &quot;jz&quot; stylized as a human on a bike" /></a></p>

    
    <footer>
      <p>
        Written on <time datetime="2022-09-05">2022-09-05</time>.
      </p>
    </footer>
    <section>
        <a href="https://comments.dorotac.eu/posts/jazda_rust">Comments</a>
        <iframe src="https://comments.dorotac.eu/posts/jazda_rust" style="width: 100%;height: 100vh;" loading="lazy">
            <a href="https://comments.dorotac.eu/posts/jazda_rust">Comments</a>
        </iframe>
    </section>
  </article>

    </main>
    <section id="global">
        <p><a href="/">dcz's projects</a></p>
        <p>Thoughts on software and society.</p>
        <div>
          <nav>
  <p>posts:</p>
  <ul>
    
        <li><a href="/posts/switch">Domain change</a></li>
    
        <li><a href="/posts/quick_maps">Maps à la carte</a></li>
    
        <li><a href="/posts/holes">Spots on the Sun and worn-out clothes</a></li>
    
        <li><a href="/posts/remote_debugging">Graphical debugging on the Librem 5 using QtCreator</a></li>
    
        <li><a href="/posts/jazda_rust">Jazda: Rust on my bike</a></li>
    
        <li><a href="/posts/why">Why Jazda?</a></li>
    
        <li><a href="/posts/git-botch-email">Git-botch-email</a></li>
    
        <li><a href="/posts/potion">A potion of experience</a></li>
    
        <li><a href="/posts/tabcheology">Browser tab archaeology</a></li>
    
        <li><a href="/posts/rust_maemo">Rust on Maemo</a></li>
    
        <li><a href="/posts/REing a pen">Reverse engineering a pen</a></li>
    
        <li><a href="/posts/in_the_middle">Start in the middle</a></li>
    
        <li><a href="/posts/nerd">When being a nerd paid off</a></li>
    
        <li><a href="/posts/flame_graph">Linux kernel flame graphs</a></li>
    
        <li><a href="/posts/fluwid">Simulating fluwids</a></li>
    
        <li><a href="/posts/blossom">A beautiful blossom of engineering excellence</a></li>
    
        <li><a href="/posts/objective">My objective function will blow it up</a></li>
    
        <li><a href="/posts/flat_earth">Geo in GL part 1: Flattening Earth</a></li>
    
        <li><a href="/posts/input_method">Wayland and input methods</a></li>
    
        <li><a href="/posts/embeddings">Word embedding fun</a></li>
    
        <li><a href="/posts/not_keyboard">It&#39;s not about keyboards</a></li>
    
        <li><a href="/posts/cargo_versions">Why I avoid Cargo: dependency versions</a></li>
    
        <li><a href="/posts/gcc_symbols">From C to exec: Part 3</a></li>
    
        <li><a href="/posts/dynamic_linking">From C to exec: Part 2</a></li>
    
        <li><a href="/posts/linking">From C to exec</a></li>
    
        <li><a href="/posts/brutalism">Brutalist blogging</a></li>
    
  </ul>
</nav>
          <p><a href="/atom.xml">Atom feed (no longer updated)</a></p>
        </div>
        <nav>
          <p>lists:</p>
          <ul>
            <li><a href="/">about me</a></li>
            <li><a href="/forges">my code</a></li>
          </ul>
        </nav>
    </section>
    <footer>
      <p>Articles published under the CC-BY-SA 4.0 license.</p>
    </footer>
  </body>
</html>