---
title: "Browser tab archaeology"
date: "2022-02-09"
---

Browser tab archaeology
==========================

Tab hoarding is a hot topic these days. There are people who feel [bad](https://fosstodon.org/@floppy/107690840449591767) about it. Even the name "hoarding" is negatively coloured. Obviously, the internet is making a big deal out of using tabs as bookmarks, because it seems like we have multiple bookmarking options, and [they all suck in practice](https://news.ycombinator.com/item?id=30166357).

But I'm shameless about it: browser tabs *are* my reading list, and browser windows are how I keep groups of links on a single topic that might be useful one day. At least until and unless a better solution emerges.

Sediments
------------

And I hope it does, cause my tab count keeps increasing! Oh, the times when I used to have 120 tabs! Long lost in the murky waters of history, tabs buried under further 100 layers of sedimentary URLs!

If only I was an archaeologist, I would order an expedition to find the gems I buried and forgot about. "Oh, that bunch of tabs comes from the period when I wanted to learn Haskell! A very valuable finding!"

If I was a tab geologist, I would order a vertical cut through the layers of rocks, look at the patterns, and guess the macro-scale processes that unconsciously guide my tab hoarding today.

Actually, maybe I am a tab geologist!

Simple Tab Groups
----------------------

I have this folder on my computer. It contains the history of my open tabs – daily snapshots going back two years. Here's an example file called `stg-backup-2022-01-23@drive4ik.json`:

```
{
    "version": "4.5.2",
    "groups": [
        {
            "id": 82,
            "title": "default",
            [...]
            "tabs": [
                {
                    "url": "https://en.wikibooks.org/wiki/Haskell",
                    "title": "Haskell - Wikibooks, open books for an open world"
                },
```

You see, I'm not the only one who uses tabs extensively. There's a lot of Firefox extensions to make them more powerful, and the one I settled on is called [Simple Tab Groups](https://addons.mozilla.org/en-US/firefox/addon/simple-tab-groups/). That's where the snapshots come from.

Excavation
-------------

Equipped with the historical snapshots and a string of Python, I dug into my own past. The entire 2 years stood in front of me, and revealed some facts that I might not have liked to know. The worst of them all is that I keep descending. When the first measurement was taken, I had merely 120 tabs open, and I peaked at 298. After days of concerted effort, I'm back to 205.

You can see it clearly here, on the age diagram:

![age graph](age.png)

It shows the general progression from prehistory on the left to modernity on the right. On the vertical scale, each non-white pixel is one open tab (yes, tabs are attached to the top of the image, fight me). Each day has a different colour assigned to it, and a tab opened on that day stays that color for its entire life.

Okay, that's not *exactly* right. Those dark horizontal streaks and dots – I think those are URLs I opened again later. Perhaps the front page of a news site, or simple the empty new tab.

As you can see, tabs opened in ancient times are assigned black, and later, differently colored layers gradually accumulate.

Clusters
----------

While the smooth colors show the long term layering, they don't show the patterns: do tabs settle down one by one, or do they get forgotten in batches?

Another colouring helps answer this. Here, neighbouring days have contrasting colours:

![days graph](days.png)

And the answer is… a single tab can get forgotten, but about as often as a bunch of tabs. I wonder if I'm special in this regard.

Prehistory
------------

But the last view still didn't shine any light on the dreaded dark layer. Thankfully, while dating is impossible, we can still analyze the material each tab was made of. i decided to colour each domain a different colour. Here's the result:

![domains graph](domains.png)

It's curious to see that there is clustering here too. What could the wide bands correspond to? A Wikipedia binge? Opening lots of comment threads on a social network? That kind of makes sense, until you notice that each wide band has a different colour – different domain! They correspond to some sorts of deep dives into topics I guess.

Statistics
-----------

A side effect of having access to that data is that I can also take some simple statistics. It turns out that, over 641 days,

- I visited 3659 URLs
- across 1213 domains,
- my last tab was usually something on [Hacker News](https://news.ycombinator.com),
- and I beat the record of tab hoarding on 2022-01-28 with 298 tabs open.

Pickaxe and brush
---------------------

I'm not one to hoard the tools of my trade, however. Any aspiring introspector can take advantage of what I did here.

You'll need Python 3 and the Pillow library. Then, [download this script](tas.py), and run it:

```
python3 tas.py ~/Downloads/my-tabs/ my_group image_name
```

Remember to replace `my_group` with the name of the tab group you wish to dig into!

So, are there any other Simple Tab Groups users here? Please show us what your hoarding looks like. If not, see you in another year!