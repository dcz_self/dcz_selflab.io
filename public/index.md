---
title: "dcz's project map"
---
dcz's projects
==============

My name is Dorota and I'm dealing with software: sometimes contributing to existing Free Software projects, sometimes experimenting with new ideas, other times making sure the forest is not lost for the trees.

My recent projects
------------------

I am selling the hackable prototype **bicycle computer** called [Jazda](https://jazda.org). Caution, there's lots of [Rust](https://www.rust-lang.org/) in that smart watch!

For [Purism](https://puri.sm), I wrote the first working **camera driver** for the Librem 5, and now I'm tasked with making the camera experience [better](https://puri.sm/posts/cameras-its-complicated/).

Also for the Librem 5, I created and maintain the virtual keyboard called [Squeekboard](https://source.puri.sm/Librem5/squeekboard/) and **Wayland text input infrastructure**.

I regularly organize workshops at local hackerspaces and present at CCC venues, most recently about Jazda at [FrosCon](https://www.froscon.org/).

Hire me!
---------------

With 9 years of experience contracting, I have some tricks up my sleeve when it comes to solving business problems.

Do you have a problem which could be solved with Free Software? Do you have a problem with Free Software itself? Perhaps you have trouble navigating the social and legal waters of Free Software?

Or perhaps you're an impactful non-profit needing software help?

I offer you an hour of **free consultations**, after which you can decide whether to hire me. Write an email to fosico [dot] dcz [at] bataty [dot] eu . *If you do not get a reply within 24h, check your spam folder or contact me on Mastodon.*

### Training

I also offer training in the form of lectures, workshops, or 1:1 mentoring on a multitude of topics. Some of them include: programming in Rust, contributing to and maintaining Free Software projects, devops, modern camera processing, basic cartography, PCB design, embedded and systems programming. Check out my blog posts in the side bar to see if you like my style: lots of diagrams, math when relevant.

All my workshops require only [Libre tools](https://en.wikipedia.org/wiki/Free_software)!

Contact
-------

I post things on Mastodon: [@dcz@fosstodon.org](https://fosstodon.org/@dcz). You can send me toots if you don't like email :)

Otherwise, use the following email address, after filling in the punctuation: fosico [dot] dcz [at] bataty [dot] eu .
