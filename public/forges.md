---
title: "dcz's contribution map"
---
dcz's public stuff
==============

Code forges
-----------

If you're interested in checking out my coding work, this list will direct you to my public accounts on different git websites - *"forges"*.

- [Work account on Purism](https://source.puri.sm/dorota.czaplejewicz) contains work done for Purism
- [Freedesktop account for Purism](https://gitlab.freedesktop.org/dcz_purism) - also Purism
- [GNOME account](https://gitlab.gnome.org/dcz) - mostly for Purism, some personal contributions
- [Personal GitHub](https://github.com/dcz-self)
- [Personal GitLab](https://gitlab.com/dcz_self)

Past work:

- [GitHub for Collabora](https://github.com/dcz-collabora)
- [Work account at Max Planck Institute](https://gitlab.mpcdf.mpg.de/czaplejewicz)
- [GitHub for MPIPZ](https://github.com/dcz-mpipz)

Presentations
-------------

- GUADEC 2018: *Wayland input methods*, *Modern computer security* (lightning talk).
- Rust Cologne: *Wayland vs Rust*.
- CCCamp 2019: *Effective Altruism* (lightning talk).

